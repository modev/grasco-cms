var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.exhibitors', {
    url: "exhibitors",
    templateUrl: 'source/modules/exhibitors/views/index.tpl.html',
    controller: 'ExhibitorsCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/exhibitors/controllers/index.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('dashboard.event.module.exhibitor', {
    url: "exhibitor",
    templateUrl: 'source/modules/exhibitor/views/exhibitor.tpl.html',
    controller: 'ExhibitorCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/exhibitors/controllers/exhibitor.js',
        ], {cache: false});
      }]
    }
  });

});
