var app = require('express')();
var bodyParser = require("body-parser");
var request = require('request');

var firebase = require('firebase').initializeApp({
    apiKey: "AIzaSyDdQAr26mxyCZFLPWk6NlxkL0TTHLMZ-v8",
    authDomain: "move-concerts.firebaseapp.com",
    databaseURL: "https://move-concerts.firebaseio.com",
    storageBucket: "move-concerts.appspot.com",
    messagingSenderId: "333553856105"
});
var _ = require('underscore');

var gcm = require('node-gcm');
var sender = new gcm.Sender('AAAATalb7mk:APA91bF2_22eYmQfOg2o5jtAF4kJxX5jfWjENy91QIY7bzD4VQRAlQQsG2V4FKqFi9V-qAmON7NjQD7sRsL1URUcChjx35_AjR55OOxOg_EGDQu0LRgDLjDt-OxZodMJD9MLU4JoWBD2');

var apn = require('apn');

var config = {
    token: {
        key:  __dirname + '/certificates/AuthKey_7QMGY4U4SS.p8',
        keyId: "7QMGY4U4SS",
      teamId: "54MALLVDA8"
    },
    production: true
  };

var service = new apn.Provider(config);
service.on("connected", function () {
    console.log("Connected");
});
service.on("transmitted", function (notification, device) {
    console.log("Notification transmitted to:" + device.token.toString("hex"));
});
service.on("transmissionError", function (errCode, notification, device) {
    console.error("Notification caused error: " + errCode);
});
service.on("timeout", function () {
    console.log("Connection Timeout");
});
service.on("disconnected", function () {
    console.log("Disconnected from APNS");
});
service.on("socketError", console.error);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

function getMessage(type, id, name) {
    var message = {
        titulo: "Move Concerts",
        payload: {}
    };

    switch (type) {
        case 'chat':
            message.mensaje = name + " te ha enviado un mensaje";
            message.payload.state = "chats";
            break;
        case 'request':
            message.mensaje = name + " te ha enviado una solicitud";
            message.payload.state = "main.networking.mycards";
            break;
        case 'answer':
            message.mensaje = name + " ha aceptado tu solicitud";
            message.payload.state = "main.networking.mycards";
            break;
    }
    return message;
}

function send_android(devices, titulo, mensaje, payload) {
    var message = new gcm.Message({'content-available': '1'});
    message.addData("title", titulo);
    message.addData("message", mensaje);
    message.addData('style', 'inbox');
    message.addData('payload', payload);
    message.addData('content-available', '1');
    sender.send(message, devices
        , function (err, res) {
            console.log(err, res);
        }
    );
}

function send_ios(devices, titulo, mensaje, payload) {
    if (devices.length < 1)
    return;

    var ios_sender = service;
    var topic_appid = "com.mocionsoft.move";
    var note = new apn.Notification(); 
    
    note.topic = topic_appid;
    note.badge = 1;
    note.contentAvailable = 1;
    note.alert = titulo + ": " + mensaje;
    note.payload = {payload: payload};
    console.log("--->>>>>> enviando push a los siguiente dispositivos", devices);
    ios_sender.send(note, devices).then(function (result) {
        console.log("sent:", result.sent.length);
        console.log("failed:", result.failed.length);
        console.log(result.failed);
    });
}

app.get("/", function (req, res) {
    return res.redirect("http://mocionsoft.com");
});

app.post('/all', function (req, res) {
    if (!req.body.titulo || !req.body.mensaje)
        return res.status(404).end();
    req.body.estado = req.body.estado || null;

    firebase.database().ref('users').once('value').then(function (snapshot) {
  
        //aca
        var android = _.map(_.filter(snapshot.val(), function (item) {
            return (item.push && item.push.type == 'android') ? true : false
        }), function (item) {
            return item.push.token
        });
        var start = 0;
        var end = 1000;
        var androidDiv=[];
        var a=0;
        
            for(i=0;i<android.length;i++){
                        if(i%1000==0){
        
                            divi = android.slice(start,end);
                            androidDiv[a] = divi;
                            start = start + 1000;
                            end = end + 1000;
                            a++;
                        }
                    } 
        androidDiv.forEach( function(valor, indice, array) {
            send_android(valor, req.body.titulo, req.body.mensaje, {state: req.body.estado});
        });
        
      
        var ios = _.map(_.filter(snapshot.val(), function (item) {
            return (item.push && item.push.type == 'ios') ? true : false
        }), function (item) {
            return item.push.token
        });
        send_ios(ios, req.body.titulo, req.body.mensaje, {state: req.body.estado});

        return res.send();
    });
});

app.post('/to', function (req, res) {
    if (!req.body.type || !req.body.id || !req.body.name)
        return res.status(404).end();

    firebase.database().ref('users').child(req.body.id).once('value').then(function (snap) {
        if (!snap.val()) return;

        var mensaje = getMessage(req.body.type, req.body.id, req.body.name);

        var android = _.map(_.where(snap.val(), {type: 'android'}), function (item) {
            return item.token;
        });
        var ios = _.map(_.where(snap.val(), {type: 'ios'}), function (item) {
            return item.token;
        });

        
        var start = 0;
        var end = 1000;
        var androidDiv=[];
        var a=0;
        
            for(i=0;i<android.length;i++){
                        if(i%1000==0){
        
                            divi = android.slice(start,end);
                            androidDiv[a] = divi;
                            start = start + 1000;
                            end = end + 1000;
                            a++;
                        }
                    } 
        
        androidDiv.forEach( function(valor, indice, array) {
           send_android(valor, mensaje.titulo, mensaje.mensaje, mensaje.payload); 
               });
        send_ios(ios, mensaje.titulo, mensaje.mensaje, mensaje.payload);

        return res.send(mensaje);
    }).catch(function (e) {
        console.error(e);
    });
});

app.get('/push', function (req, res) {
    // console.log(req.query);
    firebase.database().ref('users').child(req.query.id).once('value').then(function (snap) {
        if (!snap.val()) return;

        // var mensaje = getMessage(req.body.type, req.body.id, req.body.name);

        var android = _.map(_.where(snap.val(), {type: 'android'}), function (item) {
            return item.token;
        });
        var ios = _.map(_.where(snap.val(), {type: 'ios'}), function (item) {
            return item.token;
        });
        var start = 0;
        var end = 1000;
        var androidDiv=[];
        var a=0;
        
            for(i=0;i<android.length;i++){
                        if(i%1000==0){
        
                            divi = android.slice(start,end);
                            androidDiv[a] = divi;
                            start = start + 1000;
                            end = end + 1000;
                            a++;
                        }
                    } 

       androidDiv.forEach( function(valor, indice, array) {
             send_android(valor, 'Move Concerts', req.query.mensaje, 'home');
             
        });
        send_ios(ios, 'Move Concerts', req.query.mensaje, 'home');
       
        

        return res.send(snap.val());
    }).catch(function (e) {
        console.error(e);
    });
});

app.listen(6479, function () {
    console.log('\nPush MoveConcerts listening on port 6479!\n');
});
