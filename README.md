
#Move CMS
Aplicacion que administra el contenido de los conciertos y esta conectada con la app
Repositorios relacionados:
* Move app

# 1) Requerimientos
Para correr la aplicación es necesario contar con los siguientes programas en el computador que se piensa usar:
* npm install
* bower i
* grunt
No hay versión especifica, la aplicación se puede correr con cualquier versión, pero se recomienda la versión estable.

# 2) Versiones
No es necesaria una versión especifica para correr esta aplicación.

# 3) Instalación
Para correr eva solo es necesario clonar el repositorio, y instalar dependencias con los siguientes comandos sobre el servidor apache.
* npm install
* grunt

## 3.1) Entorno de desarrollo

Solo es necesario escribir en el navegor la la url: localhost/"nombre de la carpeta", puede variar dependiendo del puerto en que corrar el servidor.
El cms cuentas con un micro Servicio de notificaciones que se envian a los dispositivos moviles que tienen el app de move, para activar el servicio, tenemos que dirigirnos desde la consola a la carpeta pushserver y escribir en la consola el siguiente comando "node pn_config.js" para iniciar el servidor, las configuraciones para el envio de las notificaciones estan sobre este servidor



## 3.2) Entorno de producción
Tras la respectiva instalación de dependencias, es necesario revisar el archivo constant.js, ubicado en la carpeta __/src/constant.js__ y revisar las siguientes líneas.
Para generar el apk de la aplicacion ejecutar.
* gulp --cordova "prepare"
* gulp --cordova "build android"
* gulp --cordova "run android"

# 4) Sitio de deployment
Actualmente corre en el servidor de mocion

# 5) NOTAS:
Las notificaciones de android tenian un problema al envio de las notificaciones por la cantidad de usuarios a las que se les enviaba simultaneamente, por lo tanto se decidio dividir las peticion en paquetes de 1000 para no sobre cargar el servidor de firebase

# 6) Claves y accesos.
* apps@mocionsoft.com
* mocion.2040

# Source tree
**root**\
└ 404\
└ login\
└ events\
└ event\
····└ configuration\
····└ modules\
····└ publication\
····└ module\
········└ {name}\
└ assistants\
····└ pushnotifications\
└ reports\

# File tree
## **root**
└ index.html\
└ source\
····└ main.js\
····└ **404**\
········└ template.html\
····└ **login**\
········└ template.html\
········└ script.js\
····└ **events**\
* apps@mocionsoft.