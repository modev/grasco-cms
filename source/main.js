var cms = angular.module("cms", ['ui.router', 'oc.lazyLoad', 'ngStorage', 'ngFileUpload', '720kb.datepicker', 'angular.filter', 'firebase', 'leaflet-directive', 'textAngular', 'dndLists', 'angularSpinner']);

cms.run(function ($log, $rootScope, $state, $firebase, $templateCache) {

  /**
  * Eliminar cache de angular para evitar que los clientes no vean los ajustes
  * en los templates de angular
  */
  $rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
   });

  $firebase.getAuth().then(function () {
    $state.go('dashboard.events');
  }).catch(function (err) {
    $state.go('login');
  });

  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the promise is rejected
    // and redirect the user back to the login page
    if (error.code === 1) {
      $state.go('login');
    }
  });

});

cms.config(function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
  var state = $stateProvider;

  $urlRouterProvider.otherwise("/login");

  $ocLazyLoadProvider.config({
    debug: false,
    events: false
  });

});
