var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.events', {
    url: 'events',
    templateUrl: 'source/events/views/events.tpl.html',
    controller: 'EventsCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load(['source/events/controllers/events.js'], {cache: false});
      }]
    }
  });

});
