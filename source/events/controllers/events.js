var events = angular.module('events', []);

events.controller('EventsCtrl', function ($scope, $state, $firebase, $localStorage, $commons) {
    $scope.all = [];
    $scope.events = [];
    $scope.active = 0;
    $scope.error = 'none';

    $firebase.database.ref('config').child('modules').once('value', function (snapModules) {
        $scope.snapModules = snapModules.val();
    }).catch(function (err) {
        console.log('error:', err);
    });

    /**
     * Llamado a firebase de eventos de la organización.
     */
    $firebase.database.ref('events').on('child_added', function (snapEvent) {

        var event = snapEvent.val();

        if (!event) return;

        event.$id = snapEvent.getKey();
        $scope.all.push(event);
        $scope.events.push(event);
        $commons.apply($scope);
    });


    /**
     * Este metodo permite crear un evento, redireccionando hacia la configuración.
     */
    $scope.createEvent = function () {
        if (!$scope.eventName) {
            $scope.message = "Por favor ingrese un nombre para el evento";
            $scope.error = 'block';
            return;
        }

        $scope.modal = !$scope.modal;

        var eventData = {
            publish: false,
            info: { name: $scope.eventName },
            modules: $scope.snapModules || null
        };

        var key = $firebase.database.ref('events').push().key;

        $firebase.database.ref('events').child(key).update(eventData).then(function () {
            $state.go('dashboard.event.configuration', { event_id: key });
        });
    };

    /**
     * Es metodo permite eliminar un evento.
     */
    $scope.deleteEvent = function () {
        if ($scope.eventDelete != "BORRAR") {
            $scope.message = "Por favor escriba el texto correctamente";
            $scope.error = 'block';
            return;
        }
        // Logica para borrar evento

        if (!$scope.$remove_id) return;

        $firebase.database.ref('events').child($scope.$remove_id).remove();

        $scope.all.splice($scope.$remove_index, 1);
        $scope.events.splice($scope.$remove_index, 1);

        $scope.modal = !$scope.modal;
        $scope.$remove_id = null;
    };

    /**
     * Este metodo maneja el modal cerrandolo y limpiando el campo.
     */
    $scope.managerModal = function (status, id, index) {
        if (!status) {
            $scope.eventName = " ";
            $scope.eventDelete = " ";
            $scope.modal = !$scope.modal;
        } else {
            $scope.modal = !$scope.modal;
            $scope.modalType = (!id) ? 'create' : 'delete';
            $scope.$remove_id = id;
            $scope.$remove_index = index;
        }
    }

});
