var app = angular.module('terminos', []);

app.controller('terminosCtrl', function($scope, $firebase, $commons){

	var refTerminos = $firebase.database.ref('config/terms');
	
	refTerminos.on('value', function(terminos){
		$scope.htmlVariable = terminos.val();
        //console.log($scope.htmlVariable);
		$commons.apply($scope);
		
		if ($scope.$root && $scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest')
					$scope.$apply();
	});

	$scope.saveTerms = function(){
		refTerminos.set($scope.htmlVariable);
	}
})