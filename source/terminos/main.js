var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.terminos', {
    url: 'terminos',
    templateUrl: 'source/terminos/views/terminos.html',
    controller: 'terminosCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load(['source/terminos/controllers/terminos.js'], {cache: false});
      }]
    }
  });

});
