var _streaming = angular.module('cms');

_streaming.controller('streamingController', function($scope, $stateParams, $firebase, $commons, _hours, _minutes, $currentEvent) {

    $scope.hours = _hours;
    $scope.minutes = _minutes;

    $scope.currentEvent = $currentEvent;

    /**
    * Get: traer items
    */
    $scope.ref = $firebase.database.ref('streaming');
    $scope.ref.on('value', function (data) {
        $scope.sts = data.val();
        ($scope.sts === null) ? $scope.noData = true : $scope.noData = false;
        $commons.apply($scope);
    });
    /**
    * Post: Crear session type
    */
    $scope.form = {};
    $scope.modal = false;

    $scope.close = function() {
        $scope.modal = false;
        $scope.form = {};
    };
    $scope.create = function() {
        $scope.form.date = firebase.database.ServerValue.TIMESTAMP;
        $scope.ref.push($scope.form);
        $scope.close();
    };
    $scope.save = function(key, val) {
        $scope.ref.child(key).set(val);
        $scope.show = null;
    };
    /**
    * eliminar permanentemente
    */
    $scope.remove = function(key) {
        $scope.ref.child(key).remove();
    };

    /**
    * Editar session type seleccionado
    */
    $scope.edit = function(key) {
        $scope.show = key;
    };
    $scope.isCheck = function(check) {
        return $scope.show === check;
    };
});
