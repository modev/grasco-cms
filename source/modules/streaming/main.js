angular.module('cms').config(function($stateProvider){

    $stateProvider.state('dashboard.streaming', {
        url: 'streaming',
        templateUrl: 'source/modules/streaming/views/index.html',
        controller: 'streamingController',
        resolve: {
            dashboard: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'source/modules/streaming/controllers/streaming-ctrl.js'
                ], {cache: false});
            }]
        }
    });

});
