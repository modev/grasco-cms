angular.module('cms').config(function($stateProvider, $ocLazyLoadProvider){

    $stateProvider.state('dashboard.event.module.adverts', {
        url: 'banner',
        templateUrl: 'source/modules/banner/views/index.html',
        controller: 'bannerController',
        resolve: {
            dashboard: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'source/modules/banner/controllers/banner-ctrl.js'
                ], {cache: false});
            }]
        }
    });

});