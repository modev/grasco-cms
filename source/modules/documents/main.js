var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.documents', {
    url: "documents",
    templateUrl: 'source/modules/documents/views/index.tpl.html',
    controller: 'DocumentsCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/documents/controllers/index.js',
        ], {cache: false});
      }]
    }
  });

});
