var documents = angular.module('documents', []);

documents.controller('DocumentsCtrl', function($scope, $stateParams, $firebase, $commons){

  $firebase.child('documents').child($stateParams.event_id).on('value', function(snapDocuments){
    $scope.documents = snapDocuments.val();
    $commons.apply($scope);
  });

  $scope.upload = function(files){
    $commons.uploadFiles(files, function(percent){
      console.info(percent);
    }, function(response){
      angular.forEach(response, function(file){
        $firebase.child('documents').child($stateParams.event_id).push(file);
      });
    });
  }

  $scope.editDocument = function (documentId, documentName) {
    $firebase.child('documents').child($stateParams.event_id).child(documentId).child('name').set(documentName);
    $scope.modal = !$scope.modal;
  }

  $scope.deleteEvent = function (documentId) {
    $firebase.child('documents').child($stateParams.event_id).child(documentId).remove();
    $scope.modal = !$scope.modal;
  }

  $scope.managerModal = function(status, id){
    $scope.documentId = id;
    $scope.documentName = "";
    $scope.modalType = (status) ? 'edit' : 'delete';
    $scope.modal = !$scope.modal;
  }

});
