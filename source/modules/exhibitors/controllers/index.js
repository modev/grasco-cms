var exhibitors = angular.module("exhibitors", []);

exhibitors.controller("ExhibitorsCtrl", function($scope, $state, $stateParams, $localStorage, $firebase){

  $firebase.child('exhibitors').child($stateParams.event_id).on('value', function(snapExhibitors){
    $scope.exhibitors = snapExhibitors.val();
  });

});
