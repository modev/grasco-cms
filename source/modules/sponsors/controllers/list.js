var _sponsor = angular.module('sponsors');

_sponsor.controller('sponsorsListController', function ($log, $stateParams, $scope, $firebase, $commons, $currentEvent, _hours, $timeout, $state) {
    var id_evento = $stateParams.event_id;
    if (!id_evento)
        return $state.go('dashboard.events');

    var sponsorsRef = 'sponsors/' + id_evento;

    /**
     * traer sponsors
     */
    $firebase.on(sponsorsRef, function (data) {
        $scope.items = data;

        angular.forEach($scope.items, function (val, key) {
            val.$key = key;
        });
        $commons.apply($scope);
    });

    //form data
    $scope.form = {info: {}};

    $scope.add = function () {
        $scope.modal = true;
    };

    $scope.close = function () {
        $scope.modal = false;
    };

    $scope.create = function (valido) {
        $scope.loading = true;
        //validación de campo
        if (!valido) return;
        //enviar datos a firebase
        $firebase.push(sponsorsRef, $scope.form).then(function (key) {
            $timeout(function () {
                $state.go('sponsor', {sponsor_id: key});
                $scope.modal = false;
                $scope.loading = false;
            }, 1000);
        });
    };

    /**
     * eliminar permanentemente
     */
    $scope.remove = function (id) {
        $firebase.database.ref(sponsorsRef).child(id).remove();
    };

});
