var _sponsor = angular.module('sponsors', []);

_sponsor.controller('sponsorController', function ($log, $state, $stateParams, $scope, $firebase, $commons, $currentEvent, _hours, $timeout) {

    var id_evento = $stateParams.event_id;
    if (!id_evento)
        return $state.go('dashboard.events');

    var ctrl = this;

    /**
     * Get: tipos de sponsors
     */
    $scope.typeref = $firebase.database.ref('sponsortypes');
    $scope.typeref.on('value', function (data) {
        $scope.types = data.val();
        $commons.apply($scope);
    });

    /**
     * Get: datos del tipo de programa
     */
    $scope.ref = $firebase.database.ref('sponsors').child(id_evento).child($stateParams.sponsor_id);

    $scope.ref.on('value', function (data) {
        $scope.sponsor = data.val();
        $commons.apply($scope);
    });

    //guardar
    $scope.save = function (valid) {
        $scope.sponsor.valid = valid;
        $scope.sending = true;//mostar cargador
        $scope.ref.update($scope.sponsor, function (error) {
            if (error) {
                console.log('error:', error);
            } else {
            }

            $timeout(function () {
                $scope.sending = false;//ocultar cargador
            }, 1000);

            $commons.apply($scope);
        });
    };

    /**
     * eliminar permanentemente
     */
    $scope.remove = function () {
        $scope.ref.remove().then(function () {
            $state.go('sponsorsList');
        });
    };

});
