var ref = angular.module('cms');

ref.config(function($stateProvider, $ocLazyLoadProvider, $urlRouterProvider){

  // si entra directamente al view de sponsor redirect
  $urlRouterProvider.when('/event/module/sponsors', '/event/module/sponsors/sponsor-list');

  $stateProvider.state('dashboard.event.module.sponsors', {
    url: 'sponsors',
    templateUrl: 'source/modules/sponsors/views/index.tpl.html',
    controller: 'sponsorsController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/sponsors/controllers/index.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('sponsorsList', {
    url: '/sponsor-list',
    parent: 'dashboard.event.module.sponsors',
    templateUrl: 'source/modules/sponsors/views/list.tpl.html',
    controller: 'sponsorsListController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/sponsors/controllers/list.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('sponsor', {
    url: '/sponsor/{sponsor_id}',
    parent: 'dashboard.event.module.sponsors',
    templateUrl: 'source/modules/sponsors/views/edit.tpl.html',
    controller: 'sponsorController as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/sponsors/controllers/edit.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('sponsorTypes', {
    url: 'sponsor-types',
    parent: 'dashboard.event.module.sponsors',
    templateUrl: 'source/modules/sponsors/views/types.tpl.html',
    controller: 'sponsorTypesController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/sponsors/controllers/types.js',
        ], {cache: false});
      }]
    }
  });

});
