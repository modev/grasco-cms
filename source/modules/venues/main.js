var ref = angular.module('cms');

ref.config(function($stateProvider, $ocLazyLoadProvider, $urlRouterProvider){

	// si entra directamente al view de sponsor redirect
	 $urlRouterProvider.when('/event/module/venue', '/event/module/venue/create');

	$stateProvider.state('dashboard.event.module.venue', {
		url: 'venue',
		templateUrl: 'source/modules/venues/views/index.html',
		controller: function ($state) {
			$state.go('dashboard.event.module.venue.create');
		},
		resolve: {
			dashboard: ['$ocLazyLoad', function($ocLazyLoad){
				return $ocLazyLoad.load([
					'source/modules/venues/controllers/mainCtrl.js',
					'source/modules/venues/controllers/map-dir.js',
				], {cache: false});
			}]
		}
	});

	$stateProvider.state('dashboard.event.module.venue.create', {
		url: '/create',
		views: {
			'mainvenue': {
				templateUrl: 'source/modules/venues/views/create.html',
				controller: 'venueCreateCtrl as ctrl'
			},
			'sidebar': {
				templateUrl: 'source/modules/venues/views/list.html',
				controller: 'venueListCtrl as ctrl'
			}
		},
		resolve: {
			dashboard: ['$ocLazyLoad', function($ocLazyLoad){
				return $ocLazyLoad.load([
					'source/modules/venues/controllers/create-ctrl.js',
					'source/modules/venues/controllers/list-ctrl.js',
				], {cache: false});
			}]
		}
	});

	$stateProvider.state('dashboard.event.module.venue.edit', {
		url: '/edit/:venueid',
		views: {
			'mainvenue': {
				templateUrl: 'source/modules/venues/views/edit.tpl.html',
				controller: 'venueEditCtrl as ctrl'
			},
			'sidebar': {
				templateUrl: 'source/modules/venues/views/list.html',
				controller: 'venueListCtrl as ctrl'
			}
		},
		resolve: {
			dashboard: ['$ocLazyLoad', function($ocLazyLoad){
				return $ocLazyLoad.load([
					'source/modules/venues/controllers/editCtrl.js',
				], {cache: false});
			}]
		}
	});
});
