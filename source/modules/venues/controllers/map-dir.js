angular.module('cms').directive('getCords', function ($sce) {

    return {
        restrict: "EA",
        scope: {
            cords: "="
        },
        templateUrl: $sce.trustAsResourceUrl('source/modules/venues/views/mapa-dir.html'),
        controller: function ($scope) {

            $scope.$watch('cords', function() {
                if ( ($scope.cords == undefined || $scope.cords == '') ) {
                    $scope.cords = {};
                }
            });

            if ($scope.cords) {

                $scope.center = $scope.cords;

                $scope.markers = {
                    mainMarker: {
                        lat: $scope.cords.lat,
                        lng: $scope.cords.lng,
                        focus: true,
                        message: "Hey, arrastrame para obtener la ubicacion",
                        draggable: true
                    }
                }

            } else {

                $scope.center = {
                    lat: 4.654448681801623,
                    lng: -74.09385681152344,
                    zoom: 8
                };

                $scope.markers = {
                    mainMarker: {
                        lat: 4.654448681801623,
                        lng: -74.09385681152344,
                        focus: true,
                        message: "Hey, arrastrame para obtener la ubicacion",
                        draggable: true
                    }
                };
            }

            $scope.events = { // or just {} //all events
                markers:{
                    enable: [ 'dragend' ]
                    //logic: 'emit'
                }
            };

            $scope.$on("leafletDirectiveMarker.dragend", function(event, args) {
                $scope.cords.lat = args.model.lat;
                $scope.cords.lng = args.model.lng;
                $scope.cords.zoom = args.leafletObject._map._zoom;
            });

        }
    };
});
