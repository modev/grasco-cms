angular
    .module('cms')

    .controller('venueEditCtrl', function ($state, $stateParams, $timeout, _services, $scope, $commons, $log) {

        var id_evento = $stateParams.event_id;
        if(!id_evento)
            return $state.go('dashboard.events');

        var ctrl = this;

        // servicios
        ctrl.services = _services;

        //sync to firebase event's schedule
        var ref = firebase.database().ref('venues').child(id_evento).child($stateParams.venueid);

        ref.on('value', function (data) {
            ctrl.venue = data.val();
            $commons.apply($scope);
        });

        /**
         * Guardar venue
         */
        ctrl.save = function () {
            $scope.sending = true;//mostar cargador
            ref.update(ctrl.venue, function (error) {
                if (error) {
                    $log.log('error:', error);
                } else {
                }

                $timeout(function () {
                    $scope.sending = false;//ocultar cargador
                }, 1000);

                $commons.apply($scope);
            });
        };

        /**
         * Borrar venue
         */
        ctrl.delete = function () {
            ref.remove().then(function () {
                $state.go('dashboard.event.module.venue.create');
            });
        };

    })
