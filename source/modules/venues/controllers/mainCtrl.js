angular
//set venues module
	.module('cms')

	.constant('_services', {
	wifi: {
		val: 'wifi',
		name: 'Wifi',
		imgurl: '/var/www/html/4G-app/www/img/icons/Icono_Wifi.svg'
	},
	parqueadero: {
		val: 'parqueadero',
		name: 'Parqueadero',
		imgurl: '/var/www/html/4G-app/www/img/icons/Icono_parqueadero.svg'
	},
	discapacidad: {
		val: 'discapacidad',
		name: 'Discapacidad',
		imgurl: '/var/www/html/4G-app/www/img/icons/Icono_Discapacidado.svg'
	}
}).directive('resize', function ($window) {
	return function (scope, element) {
		scope.getWinHeight = function() {
			return $window.innerHeight;
		};

		var setNavHeight = function(newHeight) {
			newHeight -= 44;
			element.css('height', newHeight + 'px');
		};

		// Set on load
		scope.$watch(scope.getWinHeight, function (newValue, oldValue) {
			setNavHeight(scope.getWinHeight());
		}, true);

		// Set on resize
		angular.element($window).bind('resize', function () {
			scope.$apply();
		});
	};
});
