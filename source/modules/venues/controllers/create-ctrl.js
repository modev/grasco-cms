angular
    .module('cms')

    .controller('venueCreateCtrl', function ($state, $stateParams, $timeout, _services) {

        var id_evento = $stateParams.event_id;
        if (!id_evento)
            return $state.go('dashboard.events');

        var ctrl = this;

        // servicios
        ctrl.services = _services;

        //sync to firebase event's schedule
        var venues = firebase.database().ref('venues').child(id_evento);

        /**
         * Nuevo venue
         */
        ctrl.form = {};
        ctrl.addVenue = function () {
            venues.push(ctrl.form).then(function () {
                //success
                ctrl.form = {};
            });
        };

    });
