angular
    .module('cms')

    .controller('venueListCtrl', function ($state, $stateParams, $timeout, _services, $scope, $commons) {

        var id_evento = $stateParams.event_id;
        if (!id_evento)
            return $state.go('dashboard.events');

        var ctrl = this;

        //sync to firebase event's schedule
        firebase.database().ref('venues').child(id_evento).on('value', function (data) {
            ctrl.venues = data.val();

            angular.forEach(ctrl.venues, function (val, key) {
                val.$id = key;
            });

            $commons.apply($scope);
        });

    });
