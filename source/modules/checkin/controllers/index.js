var _speaker = angular.module('speaker', []);

_speaker.controller('CheckinController', function($scope,$state, $stateParams, $firebase, $commons, $filter){

    $scope.search = {};
    $scope.users = [];

    /**
    * traer speakers
    */
    $firebase.database.ref('users').child('n094TSX9iSVKlmU7Z2uAgVktoqU2').child('requests').on('child_added', function(data){
        if (!data.val()) return;
        $firebase.database.ref('users').child(data.getKey()).child('info').once('value', function (user) {
            $scope.users.push(user);
        });
        $commons.apply($scope);
    });

});
