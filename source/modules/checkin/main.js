var speaker = angular.module('cms');

speaker.config(function($stateProvider){

  $stateProvider.state('dashboard.checkin', {
    url: "checkin",
    templateUrl: 'source/modules/checkin/views/index.tpl.html',
    controller: 'CheckinController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/checkin/controllers/index.js'
        ], {cache: false});
      }]
    }
  });

});
