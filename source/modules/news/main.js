var surveys = angular.module('cms');

surveys.config(function ($stateProvider) {
  
  $stateProvider.state('dashboard.event.module.news', {
    abstract: true,
    url: "news",
    templateUrl: 'source/modules/news/views/index.tpl.html'
  });
  
  $stateProvider.state('dashboard.event.module.news.list', {
    url: "/",
    templateUrl: 'source/modules/news/views/list.tpl.html',
    controller: 'ListNewsController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/news/controllers/list.js',
        ], {cache: false});
      }]
    }
  });
  
  $stateProvider.state('dashboard.event.module.news.detail', {
    url: "detail/:id_notice",
    templateUrl: 'source/modules/news/views/detail.tpl.html',
    controller: 'DetailController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/news/controllers/detail.js',
        ], {cache: false});
      }]
    }
  });
  
  $stateProvider.state('dashboard.event.module.news.push', {
    url: "/push",
    templateUrl: 'source/modules/news/views/push.tpl.html',
    controller: 'PushController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/news/controllers/push.js',
        ], {cache: false});
      }]
    }
  });
  
});