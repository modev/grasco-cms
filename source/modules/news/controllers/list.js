var newsmo = angular.module('news', []);

newsmo.controller("ListNewsController", function ($scope, $firebase, $state, $commons) {

  $scope.modal = false;

  $firebase.get("news").then(function (news) {
    $scope.news = news;
    $commons.apply($scope);
  });

  $scope.create = function (title) {
    $firebase.push("news", {titulo: title}).then(function (key) {
      $state.go("dashboard.event.module.news.detail", {id_notice: key});
    });
  };

  $scope.window = function (bool) {
    $scope.modal = bool;
    $commons.apply($scope);
  };

  $scope.remove = function (id) {
    $firebase.database.ref('news').child(id).remove();
    delete $scope.news[id];
  };

});
