var newsmo = angular.module('news', []);

newsmo.controller("PushController", function ($scope, $firebase, $commons, $http) {
  
  $scope.modal = false;
  $scope.form = {};
  
  $scope.states = [
    {
      state: "main.agenda",
      nombre: "Agenda"
    },
    {
      state: "main.speakers",
      nombre: "Speakers"
    },
    {
      state: "main.sponsors",
      nombre: "Patrocinadores"
    },
    {
      state: "main.survey",
      nombre: "Encuesta"
    },
    {
      state: "main.venue",
      nombre: "Lugar de encuentro"
    },
    {
      state: "main.news",
      nombre: "Noticias"
    },
    {
      state: "main.networking.card",
      nombre: "Contactos"
    },
    {
      state: "main.livestream",
      nombre: "Live Streaming"
    }
  ]
  
  $firebase.on("notifications", function (notifications) {
    $scope.notifications = notifications;
    $commons.apply($scope);
  });
  
  $scope.window = function (bool) {
    $scope.modal = bool;
    $scope.confirm = false;
    $commons.apply($scope);
  }
  
  $scope.create = function () {
    $scope.window(false);
    $scope.form.state = $scope.states[$scope.form.state].state || null;
    $http.post("http://dev.mocionsoft.com:6481/all", { titulo: $scope.form.title, mensaje: $scope.form.message, estado : $scope.form.state});
    $firebase.push("notifications", $scope.form);
    $scope.form = {};
  }
  
});
