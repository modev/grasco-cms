var newsmo = angular.module('news', []);

newsmo.controller("DetailController", function ($log, $scope, $firebase, $stateParams, $commons, $filter, $http) {

  $scope.states = estados_push;

  $firebase.get(["news", $stateParams.id_notice]).then(function (notice) {
    $scope.notice = notice;
    console.log(notice);
    $commons.apply($scope);
  });

  $scope.save = function (valid) {
    $log.log('valid', valid);
    console.log($scope.notice);
    $scope.notice.fecha = firebase.database.ServerValue.TIMESTAMP;
    $firebase.update(["news", $stateParams.id_notice], $scope.notice).then(function () {
      sendPush($scope.notice, valid);
    });
  };

  var sendPush = function (item, valid) {
    if (!valid || !item || !item.push) return;
    item.state = ($scope.states[item.state]) ? $scope.states[item.state].state : null;
    $http.post('http://dev.mocionsoft.com:6481/all', { titulo: item.titulo, mensaje: item.pushcontent, estado: item.state});
  };

});

var estados_push = [
  {
    state: "main.home",
    nombre: "Home"
  },
  {
    state: "main.agenda",
    nombre: "Agenda"
  },
  {
    state: "main.speakers",
    nombre: "Speakers"
  },
  {
    state: "main.sponsors",
    nombre: "Patrocinadores"
  },
  {
    state: "main.survey",
    nombre: "Encuesta"
  },
  {
    state: "main.venue",
    nombre: "Lugar de encuentro"
  },
  {
    state: "main.news",
    nombre: "Noticias"
  },
  {
    state: "main.networking.card",
    nombre: "Contactos"
  },
  {
    state: "main.livestreamList",
    nombre: "Live Streaming"
  }
];
