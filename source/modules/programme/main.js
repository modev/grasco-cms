var programme = angular.module('cms');

programme.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.programme', {
    url: "programme",
    templateUrl: 'source/modules/programme/views/index.tpl.html',
    controller: 'ProgrammeController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/programme/controllers/index.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('dashboard.event.module.editProgramme', {
    url: "programme-edit/{programme_id}",
    templateUrl: 'source/modules/programme/views/edit.tpl.html',
    controller: 'ProgrammeEditController as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/programme/controllers/edit.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('dashboard.event.module.scenarios', {
    url: "scenarios",
    templateUrl: 'source/modules/programme/views/scenarios.tpl.html',
    controller: 'ScenariosController as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/programme/controllers/scenarios.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('dashboard.event.module.types', {
    url: "types",
    templateUrl: 'source/modules/programme/views/session-types.tpl.html',
    controller: 'SessionTypesController as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/programme/controllers/session-types.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('dashboard.event.module.tracks', {
    url: "tracks",
    templateUrl: 'source/modules/programme/views/tracks.tpl.html',
    controller: 'TracksController as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/programme/controllers/tracks.js',
        ], {cache: false});
      }]
    }
  });

});
