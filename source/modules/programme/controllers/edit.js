var programme = angular.module('programme', []);

programme.controller('ProgrammeEditController', function ($stateParams, $scope, $firebase, $commons, $currentEvent, _hours, _minutes) {

    var id_evento = $stateParams.event_id;
    if (!id_evento)
        return $state.go('dashboard.events');

    var ctrl = this;

    ctrl.config = {
        session: {
            val: 'session',
            name: 'Sesion',
            templateUrl: 'source/modules/programme/views/_conference.tpl.html'
        },
        conference: {
            val: 'conference',
            name: 'Conferencia',
            templateUrl: 'source/modules/programme/views/_conference.tpl.html'
        },
        brake: {
            val: 'brake',
            name: 'Pausa',
            templateUrl: 'source/modules/programme/views/_brake.tpl.html'
        }
    };
    //SET: configuración inicial para el tipo de programa
    ctrl.type = ctrl.config['session'];
    //GET: datos del evento actual
    ctrl.currentEvent = $currentEvent;
    //GET: array de horas
    $scope.hours = _hours;
    $scope.minutes = _minutes;

    //tabs y paneles de menu
    $scope.tab = 'general'; //tab activo por defecto: general

    $scope.selectTab = function (setTab) {
        $scope.tab = setTab;
    };

    //SET: configuración inicial para el tipo de programa
    ctrl.type = ctrl.config['session'];

    //GET: array de horas
    $scope.hours = _hours;

    //tabs y paneles de menu
    $scope.tab = 'general'; //tab activo por defecto: general

    $scope.selectTab = function (setTab) {
        $scope.tab = setTab;
    };

    $scope.isSelected = function (check) {
        return $scope.tab === check;
    };

    /**
     * Get: escenarios
     */
    $scope.refScenarios = $firebase.database.ref('scenarios').child(id_evento);

    $scope.refScenarios.on('value', function (data) {
        $scope.scenarios = data.val();
        ($scope.scenarios === null) ? $scope.noScenarios = true : $scope.noScenarios = false;
        $commons.apply($scope);
    });

    $scope.formScenario = {};

    $scope.newScenario = function () {
        if ($scope.formScenario.name === null) return;
        $scope.refScenarios.push($scope.formScenario);
        $scope.formScenario = {};
    };

    /**
     * Get: tipos de programa
     */
    $scope.refTypes = $firebase.database.ref('sessiontypes');

    $scope.refTypes.on('value', function (data) {
        $scope.Types = data.val();
        ($scope.Types === null) ? $scope.noTypes = true : $scope.noTypes = false;
        $commons.apply($scope);
    });

    $scope.formType = {};

    $scope.newType = function () {
        if ($scope.formType.name === null) return;
        $scope.refTypes.push($scope.formType);
        $scope.formType = {};
    };

    /**
     * Get: Tracks
     */
    $scope.refTracks = $firebase.database.ref('tracks');

    $scope.refTracks.on('value', function (data) {
        $scope.Tracks = data.val();
        ($scope.Tracks === null) ? $scope.noTracks = true : $scope.noTracks = false;
        $commons.apply($scope);
    });

    $scope.formTrack = {};

    $scope.newTrack = function () {
        if ($scope.formTrack.name === null) return;
        $scope.refTracks.push($scope.formTrack);
        $scope.formTrack = {};
    };

    /**
     * Get: datos de session
     */
    $scope.ref = $firebase.database.ref('programme').child(id_evento).child($stateParams.programme_id);
    $scope.ref.on('value', function (data) {
        $scope.session = data.val();
        $scope.session.info.date = $scope.session.info.date || ctrl.currentEvent.data.info.dateFrom;
    });

    /**
     * guardar session
     */
    $scope.saveSession = function (valid) {
        $scope.session.valid = valid;
        $scope.sending = true;//mostar cargador

        $scope.save_asistencia();

        $scope.ref.update($scope.session, function (error) {
            if (error) {
                console.log('error:', error);
            } else {
            }
            $scope.sending = false;//ocultar cargador
            $commons.apply($scope);
        });

        //$firebase.database.ref('event_speakers').child(id_evento).update($scope.session.speakers);
    };

    /**
     * Get: conferencistas
     */
    $scope.refSpeakers = $firebase.database.ref('speakers');
    $scope.refSpeakers.on('value', function (data) {
        $scope.speakers = data.val();
        angular.forEach($scope.speakers, function (val, key) {
            val.id = key;
        });
    });

    /**
     * Logica para seleccionar speakers
     */
    $scope.selectSpeaker = function (SpeakerId) {

        $scope.session.speakers = $scope.session.speakers || {};

        var result = null;
        var idx = $scope.session.speakers[SpeakerId];

        // is newly selected
        if (idx === undefined) {
            $scope.session.speakers[SpeakerId] = true;
        }
        // is currrent selected
        else {
            delete $scope.session.speakers[SpeakerId];
        }
    };
    /**
     * Logica para remover spearkers
     */
    $scope.removeSpeaker = function (SpeakerId) {
        delete $scope.session.speakers[SpeakerId];
    };

    /**
     * Asistencia
     */
    $scope.refAsistencia= $firebase.database.ref('asistencia').child(id_evento).child($stateParams.programme_id);
    $scope.refAsistencia.on('value', function (data) {
        $scope.asistencia = data.val() || {};
        $commons.apply($scope);
    });

    $scope.save_asistencia = function() {

        if ($scope.asistencia && $scope.asistencia.config && $scope.asistencia.config.active) {
            var slots = $scope.asistencia.config.slots;

            if (slots && slots.base) {
                slots.confirmed = slots.confirmed || 0;
                slots.available = slots.base - slots.confirmed;
                $scope.session.asistencia = true;
            } else
                $scope.session.asistencia = false;
        } else {
            $scope.session.asistencia = false;
        }

        $scope.refAsistencia.update($scope.asistencia, function (err) {
            if (err)
                console.log('error:', err);
        });
    }

});
