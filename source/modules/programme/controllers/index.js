var programme = angular.module('programme', []);

programme.service('$_ProgrammeConfig', function() {
    var config = this;
    config.setting = {};
});

programme.controller('ProgrammeController', function($scope,$state, $stateParams, $firebase, $commons, $currentEvent, $_ProgrammeConfig){

    var id_evento = $stateParams.event_id;
    if(!id_evento)
        return $state.go('dashboard.events');

    $scope.currentEvent = $currentEvent;
    $scope.settings = $_ProgrammeConfig;

    /**
    * Templates ng-cludes
    */
    $scope.tpl = {
        teaser: 'source/modules/programme/views/index/teaser.html'
    };

    /**
    * Funcionalidad de los tabs
    */
    //definir tab activo por defecto
    $scope.settings.panel = $scope.settings.panel || 'ViewAll';
    //fn: activa el panel seleccionado
    $scope.selectPanel = function(selected) {
        $scope.settings.panel = selected;
    };
    //fn: valida si el panel esta activo: bolean
    $scope.isSelected = function(check) {
        return $scope.settings.panel === check;
    };

    /**
    * switch: filtro por hora y escenario
    */
    $scope.settings.sort = $scope.settings.sort || false;
    $scope.setFilter = function(val) {
        $scope.settings.sort = val;
    };

    /**
    * traer tipos de sesion
    */
    $firebase.database.ref('sessiontypes').on('value', function(data){
        $scope.sessiontypes = data.val();
    });

    /**
    * traer escenarios
    */
    $firebase.database.ref('scenarios').child(id_evento).on('value', function(data){
        $scope.scenarios = data.val();
    });

    /**
    * traer programma
    */
    $firebase.database.ref('programme').child(id_evento).on('value', function(data){
        $scope.programme = data.val();
        angular.forEach($scope.programme, function(val,key) {
            val.$key = key;
        });
        $commons.apply($scope);
    });

    /**
    * Crear nueva sesion
    */
    $scope.form = { info: {} };
    $scope.type = null;

    $scope.addSession = function(type) {
        $scope.modal = true;
    };

    $scope.close = function() {
        $scope.modal = false;
    };

    $scope.create = function(valido) {
        $scope.loading = true;
        //validación de campo
        if(!valido) return;
        //enviar datos a firebase
        $firebase.database.ref('programme').child(id_evento).push($scope.form).then(function (snap) {
            var key = snap.key;
            $state.go('dashboard.event.module.editProgramme', { programme_id: key });
            $scope.modal = false;
            $scope.loading = false;
        });
    };

    /**
    * eliminar permanentemente
    */
    $scope.remove = function (id) {
        $firebase.database.ref('programme').child(id_evento).child(id).remove();
    };
});
