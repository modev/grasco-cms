var programme = angular.module('programme', []);

programme.controller('TracksController', function($scope, $stateParams, $firebase, $commons) {
    /**
    * Get: traer session types
    */
    $scope.ref = $firebase.database.ref('tracks');
    $scope.ref.on('value', function (data) {
        $scope.sts = data.val();
        ($scope.sts === null) ? $scope.noData = true : $scope.noData = false;
        $commons.apply($scope);
    });
    /**
    * Post: Crear session type
    */
    $scope.form = {};
    $scope.modal = false;

    $scope.close = function() {
        $scope.modal = false;
        $scope.form = {};
    };
    $scope.create = function() {
        $scope.ref.push($scope.form);
        $scope.close();
    };
    $scope.save = function(key, val) {
        $scope.ref.child(key).set(val);
        $scope.show = null;
    };
    /**
    * poner session type en la papelera
    */
    $scope.toTrash = function(key, val) {
        val.trash = true;
        $scope.save(key, val);
    };
    /**
    * sacar session type de la papelera
    */
    $scope.set = function(key,val) {
        val.trash = false;
        $scope.save(key, val);
    };
    /**
    * eliminar permanentemente
    */
    $scope.remove = function(key) {
        $scope.ref.child(key).remove();
    };

    /**
    * Editar session type seleccionado
    */
    $scope.edit = function(key) {
        $scope.show = key;
    };
    $scope.isCheck = function(check) {
        return $scope.show === check;
    };
});
