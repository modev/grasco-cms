var programme = angular.module('programme', []);

programme.controller('ScenariosController', function($scope, $stateParams, $firebase, $commons) {

    var id_evento = $stateParams.event_id;
    if(!id_evento)
        return $state.go('dashboard.events');

    /**
    * Get: traer escenarios
    */
    $scope.ref = $firebase.database.ref('scenarios').child(id_evento);
    $scope.ref.on('value', function (data) {
        $scope.scenarios = data.val();
        ($scope.scenarios === null) ? $scope.noData = true : $scope.noData = false;
        $commons.apply($scope);
    });
    /**
    * Post: Crear escenario
    */
    $scope.form = {};
    $scope.modal = false;

    $scope.close = function() {
        $scope.modal = false;
        $scope.form = {};
    };
    $scope.create = function() {
        $scope.ref.push($scope.form);
        $scope.close();
    };
    $scope.save = function(key, val) {
        $scope.ref.child(key).set(val);
        $scope.show = null;
    };
    /**
    * poner escenario en la papelera
    */
    $scope.toTrash = function(key, val) {
        val.trash = true;
        $scope.save(key, val);
    };
    /**
    * sacar escenario de la papelera
    */
    $scope.set = function(key,val) {
        val.trash = false;
        $scope.save(key, val);
    };
    /**
    * eliminar permanentemente
    */
    $scope.remove = function(key) {
        $scope.ref.child(key).remove();
    };

    /**
    * Editar escenario seleccionado
    */
    $scope.edit = function(key) {
        $scope.show = key;
    };
    $scope.isCheck = function(check) {
        return $scope.show === check;
    };
});
