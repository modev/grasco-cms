var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module', {
    abstract: true,
    url: "module/",
    templateUrl: 'source/modules/_init/index.tpl.html',
    controller: 'ModuleCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load(['source/modules/_init/index.js'], {cache: false});
      }]
    }
  });

});
