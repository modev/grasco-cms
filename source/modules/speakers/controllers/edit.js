var _speaker = angular.module('speaker', []);

_speaker.controller('speakerEditController', function($log, $stateParams, $scope, $firebase, $commons, $currentEvent, _hours, _minutes, $timeout, usSpinnerService){
  var ctrl = this;
  var SpeakerId = $stateParams.speaker_id;

  $scope.sending = true;
  usSpinnerService.spin('spinner-1');

    //GET: array de horas
    $scope.hours = _hours;
    $scope.minutes = _minutes;

  /**
  * Get: Tracks
  */
  $scope.refTracks = $firebase.database.ref('tracks');

  $scope.refTracks.on('value', function(data) {
    $scope.Tracks = data.val();
    ($scope.Tracks === null) ? $scope.noTracks = true : $scope.noTracks = false;
    $commons.apply($scope);
  });

  /**
  * Get: datos del tipo de programa
  */
  $scope.ref = $firebase.database.ref('speakers').child(SpeakerId);

  $scope.ref.on('value', function (data) {
      $timeout(function () {
          $scope.sending = false;//ocultar cargador
          usSpinnerService.stop('spinner-1');
      }, 1000);
    $scope.speaker = data.val();
  });

  //guardar
  $scope.save = function(valid) {
    $scope.speaker.valid = valid;
    $scope.sending = true;//mostar cargador
    $scope.ref.set($scope.speaker, function(error) {
      if(error) {
        console.log('error:', error);
      } else {}

      $timeout(function () {
        $scope.sending = false;//ocultar cargador
      }, 1000);

      $commons.apply($scope);
    });
  };

});
