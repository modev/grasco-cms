var _speaker = angular.module('speaker', []);

_speaker.controller('speakerController', function($scope,$state, $stateParams, $firebase, $commons, $currentEvent, $timeout, $q){

    $scope.search = {};
    $scope.artista = {};

    var ctrl = this;

    console.log($currentEvent);

    var event_id = $currentEvent.id;

    /**
    * traer speakers del Evento
    */
    /*$firebase.database.ref('speakers').on('value', function(data){
        $scope.speakers = data.val();
        angular.forEach($scope.speakers, function(val,key) {
            val.$key = key;
        });
        $commons.apply($scope);
    });*/
    $firebase.database.ref('event_speakers').child(event_id).on('value', function (data) {
        var promesas = {};
        angular.forEach(data.val(), function (val, key) {
            if (val)
                promesas[key] = $firebase.get(['speakers', key]);
        });

        $q.all(promesas).then(function (speakers_data) {
            $scope.speakers = speakers_data;
            angular.forEach($scope.speakers, function(val,key) {
                val.$key = key;
            });
            $commons.apply($scope);
        });
    });
    /*$firebase.get(['event_speakers', event_id]).then(function (data) {
        var promesas = {};
        angular.forEach(data, function (val, key) {
            if (val)
                promesas[key] = $firebase.get(['speakers', key]);
        });

        $q.all(promesas).then(function (speakers_data) {
            $scope.speakers = speakers_data;
            angular.forEach($scope.speakers, function(val,key) {
                val.$key = key;
            });
            $commons.apply($scope);
            /*ctrl.data = speakers_data;
            console.log(ctrl.data);*
        });
    });*/


    //form data
    $scope.form = { info: {} };

    $scope.addSpeaker = function() {
        $scope.modal = true;
    };

    $scope.deleteSpeaker = function(idSpeaker) {
        var eventRef = $firebase.database.ref('speakers').child(idSpeaker);
        var speakerRef = $firebase.database.ref('event_speakers').child(event_id).child(idSpeaker);
        eventRef.remove()
            .then(function () {
                console.log("Remove succeeded.")
            })
            .catch(function (error) {
                console.log("Remove failed: " + error.message)
            });
        speakerRef.remove()
            .then(function () {
                console.log("Remove succeeded.")
            })
            .catch(function (error) {
                console.log("Remove failed: " + error.message)
            });
    };

    $scope.close = function() {
        $scope.modal = false;
    };

    $scope.create = function(valido) {
        $scope.loading = true;
        //validación de campo
        if(!valido) return;
        //enviar datos a firebase
        $firebase.database.ref('speakers').push($scope.form).then(function (snap) {
            var key = snap.key;
            $scope.artista[key] = true;
            console.info($scope.artista);
            $firebase.database.ref('event_speakers').child($currentEvent.id).update($scope.artista);
            $timeout(function () {
                $state.go('dashboard.event.module.editspeaker', {speaker_id: key});
                $scope.modal = false;
                $scope.loading = false;
            }, 1000);
        });
    };

});
