var speaker = angular.module('cms');

speaker.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.speakers', {
    url: "speaker",
    templateUrl: 'source/modules/speakers/views/index.tpl.html',
    controller: 'speakerController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/speakers/controllers/index.js',
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('dashboard.event.module.editspeaker', {
    url: "speaker-edit",
    templateUrl: 'source/modules/speakers/views/edit.tpl.html',
    controller: 'speakerEditController as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/speakers/controllers/edit.js',
        ], {cache: false});
      }]
    },
    params: { speaker_id: null }
  });

});
