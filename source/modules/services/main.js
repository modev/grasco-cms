var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.services', {
    url: "services",
    templateUrl: 'source/modules/services/views/index.tpl.html',
    controller: 'ServicesCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/services/controllers/index.js',
        ], {cache: false});
      }]
    }
  });

});
