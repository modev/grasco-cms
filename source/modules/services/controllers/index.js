var services = angular.module('services', []);

services.controller('ServicesCtrl', function($scope, $stateParams, $firebase, $commons){

  $firebase.child('services').child($stateParams.event_id).on('value', function(snapServices){
    $scope.services = snapServices.val();
  });

  $scope.edit = function(key, formEdit){

  }

  $scope.delete = function(key){

  }

});
