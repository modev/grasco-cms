var gallery = angular.module('cms');

gallery.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.gallery', {
    url: "gallery",
    templateUrl: 'source/modules/gallery/views/index.tpl.html',
    controller: 'GalleryController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/gallery/controllers/index.js'
        ], {cache: false});
      }]
    }
  });

  $stateProvider.state('dashboard.event.module.editGallery', {
    url: "gallery-edit/{gallery_id}",
    templateUrl: 'source/modules/gallery/views/edit.tpl.html',
    controller: 'GalleryEditController as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/gallery/controllers/edit.js'
        ], {cache: false});
      }]
    }
  });

});
