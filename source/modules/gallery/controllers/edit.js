var gallery = angular.module('gallery');

gallery.controller('GalleryEditController', function($stateParams, $scope, $firebase, $commons, $currentEvent){

    var ctrl = this;

    var id_evento = $stateParams.event_id;

    if (!id_evento)
        return $state.go('dashboard.events');

    ctrl.gallery = $stateParams.gallery_id;

    // GET: datos del evento actual
    ctrl.currentEvent = $currentEvent;

    $scope.ref = $firebase.database.ref('gallery').child(id_evento).child(ctrl.gallery);

    /**
     * Three way data binding
     */
    $scope.gallery = $scope.gallery || {};
    $scope.ref.on('value', function (data) {
        $scope.gallery = data.val();
        $commons.apply($scope);
    });

    /**
     * Guardar galería
     * @param key
     * @param val
     */
    $scope.save = function (key, val) {
        //mostar cargador
        $scope.sending = true;
        $scope.ref.update($commons.objectClean($scope.gallery), function (error) {
            if (error) {
                console.log('error:', error);
            } else {}
            //ocultar cargador
            $scope.sending = false;
            $commons.apply($scope);
        });
    };

    /**
     * logica Multiples Imagenes
     */
    $scope.gallery.images = $scope.gallery.images || [];
    var imgBaseObj = {
        caption: ''
    };

    $scope.addImage = function () {
        $scope.gallery.images.push({});
    };

    $scope.removeImage = function (index) {
        $scope.gallery.images.splice(index, 1);
    };
});
