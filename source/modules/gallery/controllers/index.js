var gallery = angular.module('gallery', []);

gallery.service('$_GalleryConfig', function() {
    var config = this;
    config.setting = {};
});

gallery.controller('GalleryController', function($scope,$state, $stateParams, $firebase, $commons, $currentEvent, $_GalleryConfig){

    var id_evento = $stateParams.event_id;
    $scope.currentEvent = $currentEvent;
    $scope.settings = $_GalleryConfig;

    /**
    * Templates ng-cludes
    */
    $scope.tpl = {
        teaser: 'source/modules/gallery/views/index/teaser.html'
    };

    /**
    * Funcionalidad de los tabs
    */
    //definir tab activo por defecto
    $scope.settings.panel = $scope.settings.panel || 'ViewAll';
    //fn: activa el panel seleccionado
    $scope.selectPanel = function(selected) {
        $scope.settings.panel = selected;
    };
    //fn: valida si el panel esta activo: bolean
    $scope.isSelected = function(check) {
        return $scope.settings.panel === check;
    };

    /**
    * switch: filtro por hora y escenario
    */
    $scope.settings.sort = $scope.settings.sort || false;
    $scope.setFilter = function(val) {
        $scope.settings.sort = val;
    };

    /**
    * traer galerias
    */
    $firebase.database.ref('gallery').child(id_evento).on('value', function(data){
        $scope.programme = data.val();
        angular.forEach($scope.programme, function(val,key) {
            val.$key = key;
        });
        $commons.apply($scope);
    });

    /**
    * Crear nueva galeria
    */
    $scope.form = { info: {} };
    $scope.type = null;

    $scope.addSession = function(type) {
        $scope.modal = true;
    };

    $scope.close = function() {
        $scope.modal = false;
    };

    $scope.create = function(valido) {
        $scope.loading = true;
        //validación de campo
        if(!valido) return;
        //enviar datos a firebase
        $firebase.database.ref('gallery').child(id_evento).push($scope.form).then(function (snap) {
            var key = snap.key;
            $state.go('dashboard.event.module.editGallery', { gallery_id: key });
            $scope.modal = false;
            $scope.loading = false;
        });
    };

    /**
    * eliminar permanentemente
    */
    $scope.remove = function (id) {
        $firebase.database.ref('gallery').child(id_evento).child(id).remove();
    };
});
