var surveys = angular.module('surveys', []);

surveys.controller("SurveysController", function ($scope, $firebase, $commons, $state) {
  
  $scope.modal = false;
  
  $firebase.on(['event', 'survey'], function (sur) {
    $scope.surveyActive = sur;
    $commons.apply($scope);
  });
  
  $firebase.get(['surveys', 'polls', 'event']).then(function (surveys) {
    $scope.surveys = surveys;
    $commons.apply($scope);
  });
  
  $firebase.get(['surveys', 'types']).then(function (tipos) {
    $scope.tipos = tipos;
  });
  
  $scope.window = function (bool) {
    $scope.modal = bool;
    $commons.apply($scope);
  }
  
  $scope.create = function (pollname) {
    $firebase.push(['surveys', 'polls', 'event'], {
      name: pollname,
      public: false
    }).then(function (id) {
      $state.go("dashboard.event.module.survey", {idSurvey: id})
    });
  }
  
  $scope.changeSurvey = function (poll_key) {
    if($scope.surveyActive == poll_key)
      $firebase.set(['event', 'survey'], false);
    else
      $firebase.set(['event', 'survey'], poll_key);
  }
  
});