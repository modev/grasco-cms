var surveys = angular.module('surveys', []);

surveys.controller("SurveyReporteController", function ($scope, $firebase, $commons, $stateParams) {

  $firebase.get(['surveys', 'types']).then(function (types) {
    $firebase.on(['surveys', 'polls', 'event', $stateParams.idSurvey], function (poll) {
      angular.forEach(poll.results, function (item, id_ask) {
        poll.results[id_ask] = {
          answers: item,
          ask: poll.questions[id_ask].ask,
          type: types[poll.questions[id_ask].type]
        }

        if (poll.results[id_ask].type == "unica" || poll.results[id_ask].type == "multiple") {
          poll.results[id_ask] = $scope.tipoUno(poll.results[id_ask], poll.questions[id_ask].answers);
        } else {
          poll.results[id_ask] = $scope.tipoDos(poll.results[id_ask], poll.questions[id_ask].answers);
        }
      });
      $scope.pollName = poll.name;
      $scope.results = poll.results;
      $commons.apply($scope);
    });
  });

  $scope.tipoUno = function (item, answers) {
    //Total
    item.total = 0;
    for (var i in item.answers) {
      item.total += item.answers[i];
    }

    //Answers porcents
    item.porcents = {};
    item.names = {};
    for (var i in item.answers) {
      item.porcents[i] = (item.answers[i] * 100) / item.total;
      item.names[i] = answers[i];
    }
    return item;
  }
  
  $scope.tipoDos = function (item, answers) {
    var newValue = {
      type: "orden",
      ask: item.ask,
      length: Object.keys(answers).length,
      total: 0,
      answers: []
    };
    
    angular.forEach(item.answers, function (array_votes, key) {
      var temp = {};
      temp.answer = answers[key];
      temp.total = array_votes.reduce(function (a,b) { return a + b }, 0);
      temp.votes = array_votes;
      temp.percentages = array_votes.map(function (votes) { if(temp.total == 0) return 0; else return (votes * 100) / temp.total });
      newValue.answers.push(temp);
    });
    
    return newValue;
  }

});

/*
{
  "ask":"Prueba orden",
  "length":4,
  "total":0,
  "answers":[
    {
      "answer":"Item 1",
      "total":0,
      "votes":[0,0,0,0],
      "percentages":[0,0,0,0]
    }
  ]
}
*/