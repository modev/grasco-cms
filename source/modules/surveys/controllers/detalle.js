var surveys = angular.module('surveys', []);

surveys.controller("SurveyController", function ($scope, $firebase, $stateParams, $commons) {
  
  $scope.id_poll = $stateParams.idSurvey;
  
  $firebase.on(['surveys', 'polls', 'event', $stateParams.idSurvey], function (poll) {
    $scope.poll = poll;
    $commons.apply($scope);
  });
  
  $firebase.get(['surveys', 'types']).then(function (types) {
    $scope.types = types;
    $commons.apply($scope);
  });
  
  $scope.create = function (ask, type) {
    $firebase.push(['surveys', 'polls', 'event', $stateParams.idSurvey, 'questions'], {ask: ask, type: type});
    $scope.modal = false;
    $commons.apply($scope);
  }
  
  $scope.saveQuestions = function () {
    $firebase.set(['surveys', 'polls', 'event', $stateParams.idSurvey], $scope.poll);
  }
  
  $scope.removeQuestion = function (key) {
    $firebase.remove(['surveys', 'polls', 'event', $stateParams.idSurvey, 'questions', key]);
  }
  
  $scope.newAnswer = function (key, type) {
    $firebase.push(['surveys', 'polls', 'event', $stateParams.idSurvey, 'questions', key, 'answers'], "Nueva Respuesta").then(function (fbkey) {
      if(type == 'unica' || type == 'multiple') {
        $firebase.set(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', key, fbkey], 0);
      } else {
        $scope.addOrderAnswer(key, fbkey);
      }  
    });
  }
  
  $scope.removeAnswer = function (id_question, id_answer, type) {
    $firebase.remove(['surveys', 'polls', 'event', $stateParams.idSurvey, 'questions', id_question, 'answers', id_answer]);
    if(type == 'unica' || type == 'multiple') {
      $firebase.remove(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', id_question, id_answer]);
    } else {
      $scope.removeOrderAnswer(id_question, id_answer);
    }
  }
  
  $scope.addOrderAnswer = function (id_question, id_answer) {
    $firebase.get(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', id_question]).then(function (question) {
      if(question == null){
        $firebase.set(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', id_question, id_answer], {0: 0});
      } else {
        var numAnswers = Object.keys(question).length; console.info();
        angular.forEach(question, function (answer, key_answer) {
          question[key_answer][numAnswers] = 0;
        });
        question[id_answer] = {}
        for(var i = 0; i <= numAnswers; i++) {
          question[id_answer][i] = 0;
        }
        $firebase.set(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', id_question], question);
      }
    });
  }
  
  $scope.removeOrderAnswer = function (id_question, id_answer) {
    $firebase.remove(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', id_question, id_answer]).then(function () {
      $firebase.get(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', id_question]).then(function (question) {
        var numAnswers = Object.keys(question).length;
        angular.forEach(question, function (item, key_answer) {
          delete question[key_answer][numAnswers];
        });
        $firebase.set(['surveys', 'polls', 'event', $stateParams.idSurvey, 'results', id_question], question);
      });
    });
  }
  
  $scope.window = function (bool) {
    $scope.modal = bool;
    $commons.apply($scope);
  }
  
});