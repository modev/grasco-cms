var surveys = angular.module('cms');

surveys.config(function ($stateProvider) {
  
  $stateProvider.state('dashboard.event.module.surveys', {
    url: "surveys",
    templateUrl: 'source/modules/surveys/views/index.tpl.html',
    controller: 'SurveysController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/surveys/controllers/index.js',
        ], {cache: false});
      }]
    }
  });
  
  $stateProvider.state('dashboard.event.module.survey', {
    url: "surveys/:idSurvey",
    templateUrl: 'source/modules/surveys/views/detalle.tpl.html',
    controller: 'SurveyController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/surveys/controllers/detalle.js',
        ], {cache: false});
      }]
    }
  });
  
  $stateProvider.state('dashboard.event.module.surveyreport', {
    url: "surveys/:idSurvey/reportes",
    templateUrl: 'source/modules/surveys/views/reporte.tpl.html',
    controller: 'SurveyReporteController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/surveys/controllers/reporte.js',
        ], {cache: false});
      }]
    }
  });
  
});