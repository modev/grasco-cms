var modules = angular.module('modules', []);

modules.controller("ModuleCtrl", function($scope, $state, $stateParams, $firebase, $commons){
  /**
   * Este condicional permite evaluar si existen las variables para ejecución, de lo contrario devuelve a los eventos de la organización.
  */
  var id_evento = $stateParams.event_id;

  if(!id_evento)
    return $state.go('dashboard.events');

  $firebase.database.ref('events').child(id_evento).child('modules').on('value', function(snapModules){
    $scope.modules = snapModules.val().menu;
    if(snapModules.val().tabs)
      $scope.modules.concat(snapModules.val().tabs);
    $commons.apply($scope);
  });


});
