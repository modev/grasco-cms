var newsmo = angular.module('wall', []);

newsmo.controller("ListNewsController", function ($scope, $firebase, $state, $filter, $stateParams, $commons, usSpinnerService, $timeout, $currentEvent) {


  var ctrl = this;
  $scope.likes = {};
    $scope.sending = true;//mostar cargador
    usSpinnerService.spin('spinner-1');
  var id_evento = $stateParams.event_id;
  if(!id_evento)
        return $state.go('dashboard.events');

    $scope.ordenPropiedadMusica = 'likes';
    $scope.ordenDireccionMusica = -1;
    $scope.ordenMusica = (($scope.ordenDireccionMusica === -1)?"+":"")+$scope.ordenPropiedadMusica;


    $scope.cambiarOrdenMusica = function(propiedadmusica){
        $scope.ordenPropiedadMusica = propiedadmusica;
        $scope.ordenDireccionMusica *= -1;
        $scope.ordenMusica = (($scope.ordenDireccionMusica === -1)?"-":"")+propiedadmusica;
    };

  /**
    * traer wall
  */
  var wallRef = firebase.database().ref('wall').child(id_evento);
  var likesRef = firebase.database().ref('users_likes');
  likesRef.on('value', function (likes) {
      angular.forEach(likes.val(), function (val, key) {
        $scope.likes[key] = val['post_likes'];
      });
  });

  wallRef.once('value').then(function (list) {
      $scope.walls = list.val();
      $timeout(function () {
          $scope.sending = false;//ocultar cargador
          usSpinnerService.stop('spinner-1');
      }, 1000);
      angular.forEach($scope.walls, function (val) {
          $scope.date = new Date(val.date);
          var format = 'dd-MMM-yy';
          val.fecha = $filter('date')($scope.date, format);
      });
      $commons.apply($scope);
  });

  $scope.openmodal = function(id) {
      $scope.lista = [];
      $scope.post = $scope.walls[id];
      angular.forEach($scope.likes, function (val, key) {
          if(val.hasOwnProperty(id)){
              $firebase.get(['users', key]).then(function (data) {
                  $scope.lista.push((data.info.email) ? data.info.email : ' ');
              });
          }
      });
      console.log($scope.lista);
      $scope.modal = true;
  };

  $scope.close = function () {
      $scope.modal = false;
  };


  /**
   * Fecha formateada
   */
  ctrl.getDateFormat = function (date) {
    return moment().to(date);
  };

  $scope.remove = function (id) {
    wallRef.child(id).update({'status': false});
  };

  $scope.post = function (id, status) {
    //console.info(status);
    //wallRef.child(id).update({'status': true});
  };

  $scope.setStatus = function (id, status) {
    if (status){
      wallRef.child(id).update({'status': false});
    }else{
      wallRef.child(id).update({'status': true});
    }
    //console.info(status);
    //wallRef.child(id).update({'status': true});
  };




  

});
