var surveys = angular.module('cms');

surveys.config(function ($stateProvider) {
  
  $stateProvider.state('dashboard.event.module.wall', {
    abstract: true,
    url: "wall",
    templateUrl: 'source/modules/wall/views/index.tpl.html'
  });
  
  $stateProvider.state('dashboard.event.module.wall.list', {
    url: "/",
    templateUrl: 'source/modules/wall/views/list.tpl.html',
    controller: 'ListNewsController',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/wall/controllers/list.js',
        ], {cache: false});
      }]
    }
  });
  
});