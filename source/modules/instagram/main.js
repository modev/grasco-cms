var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.instagram', {
    url: "instagram",
    templateUrl: 'source/modules/instagram/views/index.tpl.html',
    controller: 'InstagramCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/instagram/controllers/index.js',
        ], {cache: false});
      }]
    }
  });

});
