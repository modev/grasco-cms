var instagram = angular.module('instagram', []);

instagram.controller('InstagramCtrl', function($scope, $stateParams, $firebase, $commons){

  var feed = null;

  $firebase.child('instagram').child($stateParams.event_id).on('value', function(snapInstagram){
    document.getElementById('instagram').innerHTML = '';
    $scope.text = snapInstagram.val();

    if(!$scope.text){
      return;
    } else if ($scope.text.substring(0, 1) == '#' && $scope.text.substring(1) != "") {
      $scope.getByHashtag($scope.text.substring(1));
    } else if ($scope.text.substring(0, 1) == '@' && $scope.text.substring(1) != "") {
      $commons.getInstagramId($scope.text.substring(1), function(userId){
        $scope.getByUser(userId);
      });
    }
  });

  $scope.save = function(text){
    $firebase.child('instagram').child($stateParams.event_id).set(text);
  }

  $scope.getByHashtag = function(hashtag){
    feed = new Instafeed({
      target: "instagram",
      clientId: "91bd5285f5914748ae8ee7ac02707f1d",
      get: 'tagged',
      tagName: hashtag,
      template: "<img src='{{image}}' />",
      limit: 10,
      resolution: "low_resolution",
    });

    feed.run();
  }

  $scope.getByUser = function(userId){
    feed = new Instafeed({
      target: "instagram",
      clientId: "91bd5285f5914748ae8ee7ac02707f1d",
      accessToken: "1547019221.91bd528.8144cf51806f42aa86cda069326aedc7",
      get: 'user',
      userId: userId,
      template: "<img src='{{image}}' class='instaphoto' />",
      limit: 10,
      resolution: "low_resolution",
    });

    feed.run();
  }

  $scope.more = function(state){
    feed.next();
  }

});
