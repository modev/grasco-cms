var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.certificado', {
    url: 'certificado',
    templateUrl: 'source/modules/certificado/views/certificado.html',
    controller: 'CertificadoCtrl as ctrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad) {
        return $ocLazyLoad.load(['source/modules/certificado/controllers/certificado.js'], {cache: false});
      }]
    }
  });

});
