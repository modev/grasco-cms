var app = angular.module('cms');

app.controller('CertificadoCtrl', function($scope, $firebase, $commons, event_id) {

    /**
     * Get: datos de certificado
     */
    $scope.certificado = {};
    $scope.test = {};

    $scope.ref = $firebase.database.ref('certificados').child(event_id);
    $scope.ref.on('value', function (data) {
        $scope.certificado = data.val() || {};
        $commons.apply($scope);
    });

    /**
     * guardar certificado
     */
    $scope.save = function () {
        $scope.sending = true;//mostar cargador
        $scope.ref.update($scope.certificado, function (error) {
            if (error) {
                console.log('error:', error);
            } else {
            }
            $scope.sending = false;//ocultar cargador
            $commons.apply($scope);
        });
    };

    /**
     * funcionalidad tabs
     * @type {string}
     */
    $scope.tab = 'asistencia'; //tab activo por defecto: general

    $scope.selectTab = function (setTab) {
        $scope.tab = setTab;
    };

    $scope.isSelected = function (check) {
        return $scope.tab === check;
    };

});