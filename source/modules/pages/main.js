var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.event.module.pages', {
    url: "pages",
    templateUrl: 'source/modules/pages/views/index.tpl.html',
    controller: 'PagesCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/pages/controllers/index.js',
        ], {cache: false});
      }]
    },
    params: {
      page_id: null
    }
  });

});
