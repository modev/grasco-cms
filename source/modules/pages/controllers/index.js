var pages = angular.module('pages', []);

pages.controller('PagesCtrl', function($scope, $stateParams, $firebase, $commons){
  // $stateParams.page_id $stateParams.event_id

  $firebase.child('pages').child($stateParams.event_id).child($stateParams.page_id).on('value', function(snapPage){
    $scope.page = snapPage.val();
  });

});
