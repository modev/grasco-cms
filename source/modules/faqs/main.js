var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.faqs', {
    url: "faqs",
    templateUrl: 'source/modules/faqs/views/index.tpl.html',
    controller: 'FaqsCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load([
          'source/modules/faqs/controllers/index.js'
        ], {cache: false});
      }]
    }
  });

});
