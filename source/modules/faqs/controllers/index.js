var faqs = angular.module('faqs', []);

faqs.controller('FaqsCtrl', function ($scope, $stateParams, $commons) {

    var ref = firebase.database().ref('faqs');

    ref.on('value', function (snapFaqs) {
        $scope.faqs = snapFaqs.val();
        $commons.apply($scope);
    });

    $scope.form = {};

    $scope.addFaq = function () {
        $scope.newFaq = !$scope.newFaq;
        ref.push($scope.form);
        $scope.form = {};
    };

    $scope.deleteFaq = function (key) {
        ref.child(key).remove();
    };

    $scope.save = function () {
        ref.set($scope.faqs);
    };

});
