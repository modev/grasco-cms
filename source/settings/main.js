var cms = angular.module('cms');

cms.config(function ($stateProvider) {

    $stateProvider.state('dashboard.settings', {
        url: "settings",
        templateUrl: 'source/settings/views/setting-tpl.html',
        controller: 'SettingsCtrl',
        resolve: {
            dashboard: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load(['source/settings/controllers/settings-ctrl.js'], {cache: false});
            }]
        }
    });

});
