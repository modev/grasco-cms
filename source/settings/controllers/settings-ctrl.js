var settings = angular.module('cms');

settings.controller('SettingsCtrl', function ($scope, $firebase, $commons) {

    var ref = $firebase.database.ref('settings');

    ref.on('value', function (snap) {
        $scope.settings = snap.val();
        $commons.apply($scope);
    });

    $scope.save = function () {
        if ($scope.settings)
            ref.update($scope.settings);
    }

});
