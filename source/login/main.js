var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('login', {
    url: "/login",
    templateUrl: 'source/login/login.tpl.html',
    controller: 'LoginCtrl',
    resolve: {
      login: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load(['source/login/login.js'], {cache: false});
      }]
    }
  });

});
