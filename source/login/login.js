var login = angular.module('login', []);

login.controller('LoginCtrl', function($scope, $state, $firebase, $localStorage, $commons){
  $scope.form = {};
  $scope.error = 'none';

  $scope.submit = function(){
    $firebase.login($scope.form.email, $scope.form.password).then(function(auth) {
      $scope.form = {};
      $state.go('dashboard.event.configuration');
    }, function (error) {
      $scope.showError("Email o contraseña incorrectos, por favor verifíquelos e intente nuevamente.");
    });
  }

  $scope.showError = function(message){
    $scope.error = 'block';
    $scope.message = message;
    $commons.apply($scope);
  }

});
