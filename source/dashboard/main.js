var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard', {
    abstract: true,
    url: "/",
    templateUrl: 'source/dashboard/dashboard.tpl.html',
    resolve: {
      check: function ($firebase){
        return $firebase.getAuth();
      }
    }
  });

});
