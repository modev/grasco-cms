/**
 * toNull Directive
 * Esta directiva permite cambiar un string vacio por nulo
 */
angular.module('cms').directive('toNull', function($parse) {
 return {
  restrict: 'A',
  require: '?ngModel',
  link: function(scope, element, attrs, ngModel) {
   var ngModelGet = $parse(attrs.ngModel);

   //ver cuando hay un cambio en el ng-model
   scope.$watch(attrs.ngModel, function() {
    if ( (ngModelGet(scope) == undefined || ngModelGet(scope) == '') && angular.isObject(ngModel) ) {
     var model = $parse(attrs.ngModel);
     model.assign(scope, null);
    }
   });
  }
 };
})
