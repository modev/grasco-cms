var app = angular.module('cms');

app.directive('uploadImagen', function ($log, $firebase) {

    //generar id
    var guid = function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1)
                .trim().replace(/^\d+/, '');
        }
        return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
    };

    var controller = ['$scope', function ($scope) {

        //Valores por defecto
        if (!$scope.propiedadimagen)
            $scope.propiedadimagen = 'fullsize';
        if (!$scope.ancho)
            $scope.ancho = '414';
        if (!$scope.alto)
            $scope.alto = '264';

        $scope.upload = function (files, section) {

            if (!files || files[0] == undefined) return;

            // usamos el motodo guid para evitar tener nombres de imagenes repetidos
            files[0].$ngfName = guid() + files[0].$ngfName;

            $firebase.upload('speakers', files[0]).then(function (res) {
                // check modelo
                $scope.modelo = $scope.modelo || {};
                // asignacion
                $scope.modelo[section] = res;
            });
        };

        $scope.deleteImg = function () {
            $scope.modelo = {};
        };

        function init () {
            $scope.items = angular.copy($scope.datasource);
        }

        init();

        $scope.addItem = function () {
            $scope.add();

            //Add new customer to directive scope
            $scope.items.push({
                name: 'New Directive Controller Item'
            });
        };
    }];

    return {
        scope: {
            modelo: '=', //Two-way data binding
            ancho: '@',
            alto: '@',
            propiedadimagen: '@'
        },
        controller: controller,
        templateUrl: 'source/commons/directives/upload-image/upload-image.html'
    };
});
