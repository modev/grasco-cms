var app = angular.module('cms');

app.directive('uploadPdf', function ($log, $firebase) {

    //generar id
    var guid = function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1)
                .trim().replace(/^\d+/, '');
        }
        return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
    };

    var controller = ['$scope', function ($scope) {

        $scope.loading = false;
        $scope.upload = function (files) {

            if (!files || files[0] == undefined) return;
            $scope.loading = true;
            // usamos el motodo guid para evitar tener nombres de archivos repetidos
            files[0].$ngfName = new Date().getTime() + files[0].name;

            var temp_name = files[0].name;

            $firebase.upload('pdf', files[0]).then(function (res) {
                $scope.archivo = $scope.archivo || {};
                $scope.archivo.name = temp_name;
                $scope.archivo.url = res;
                $scope.memoria = true;
                $scope.loading = false;
            });
        };

        $scope.deletePDF = function () {
            $scope.archivo = {};
            $scope.memoria = null;
        };
    }];

    return {
        scope: {
            archivo: '=', //Two-way data binding
            memoria: '='
        },
        controller: controller,
        templateUrl: 'source/commons/directives/upload-pdf-file/upload-pdf.html'
    };
});
