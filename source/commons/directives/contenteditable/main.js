angular.module('cms').directive('contenteditable', function ($timeout) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            // view -> model
            elm.bind('blur', function () {
                scope.$apply(function () {
                    ctrl.$setViewValue(elm.html());
                });
            });

            // model -> view
            ctrl.$render = function () {
                elm.html(ctrl.$viewValue);
            };

            // load init value from DOM
            $timeout(function () {
                ctrl.$setViewValue(elm.html());
            }, 10);
        }
    };
});
