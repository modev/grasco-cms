var uploadImage = angular.module('cms');

/*
 * Uso de la directiva:
 * <upload atributo="valor"></upload>
 * @attribute
 */
uploadImage.directive('upload', ['Upload', '$commons', function (Upload, $commons) {
  return {
    restrict: 'EA',
    templateUrl: 'source/commons/directives/upload/upload.tmp.html',
    require: "?ngModel",
    scope: {},
    link: function ($scope, element, attrs, ngModelCtrl) {
      var div = angular.element( document.querySelector('#upload-wrapper') );

      if(attrs.type)
        div.attr("ngf-accept", (attrs.type == 'pdf') ? 'application/pdf' : ((attrs.type == 'image') ? 'image/*' : ((attrs.type == 'csv') ? '.csv' : '')));
      if(attrs.multiple)
        div.attr("ngf-multiple", (attrs.multiple == "") ? true : attrs.multiple);
      if(attrs.minWeight)
        div.attr("ngf-min-size", attrs.minWeight + 'KB');
      if(attrs.maxWeight)
        div.attr("ngf-max-size", attrs.maxWeight + 'KB');
      if(attrs.minHeight)
        div.attr("ngf-min-height", attrs.minHeight);
      if(attrs.maxHeight)
        div.attr("ngf-max-height", attrs.maxHeight);
      if(attrs.minWidth)
        div.attr("ngf-min-width", attrs.minWidth);
      if(attrs.maxWidth)
        div.attr("ngf-max-width", attrs.maxWidth);

    }
  };
}]);
