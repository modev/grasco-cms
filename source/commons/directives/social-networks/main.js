var _redes = angular.module('cms');

_redes.directive('socialNetworks', function ($sce) {
    return {
        restrict: 'EA',
        scope: {
            model: '='
        },
        templateUrl: $sce.trustAsResourceUrl('source/commons/directives/social-networks/index.html'),
        controller: function ($log, $scope, $firebase, $commons) {

            $scope.model = $scope.model || {};

            $scope.$watch('model', function() {
                if ( ($scope.model == undefined || $scope.model == '') ) {
                    $scope.model = {};
                }
            });

            /**
            * Redes:
            * Funcionalidad tabs
            */
            $scope.ref = $firebase.database.ref('socialNetworks');
            $scope.ref.on('value', function(data) {
                $scope.Networks = data.val();
                $commons.apply($scope);
            });


            $scope.network = null;//guardamos red social activada
            //funcion determina cual red social esta activa
            $scope.selectNetwork = function(setNetwork) {
                $scope.network = setNetwork;
            };
            //revisamos si la red social esta activa y retornamos in valos boleano
            $scope.isSelected = function(checkNetwork) {
                return $scope.network === checkNetwork;
            };
        }
    }
});
