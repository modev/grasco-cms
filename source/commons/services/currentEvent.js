angular.module('cms').service('$currentEvent', function($filter) {
    var currentEvent = this;

    /**
    * Function: permite calcular el numero de dias ingresando la fecha inicial y final
    */
    function getDates(startDate, stopDate) {
        var dateArray = [];
        var currentDate = new Date(startDate);
        stopDate = new Date(stopDate);

        while (currentDate <= stopDate) {
            dateArray.push($filter('date')(currentDate, 'MMM d, y'));
            currentDate.setDate(currentDate.getDate() + 1);
        }
        return dateArray;
    }

    currentEvent.data = {};
    currentEvent.id = null;
    currentEvent.valid = {};
    currentEvent.multiple = false;
    currentEvent.dates = null;
    currentEvent.checkDates = function() {
        /**
        * Reset vars
        */
        currentEvent.multiple = false;
        currentEvent.dates = null;

        /**
        * Esta condición evalua si el evento es de multiple dias
        * de ser así se calculan los dias
        */
        var dateFrom = currentEvent.data.info.dateFrom, dateTo = currentEvent.data.info.dateTo;

        if(!dateTo) return;
            currentEvent.multiple = true;
            currentEvent.dates = getDates(dateFrom, dateTo);
    };
});
