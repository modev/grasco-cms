angular.module("cms").factory("$firebase", function($log, $q) {
  var config = {
    apiKey: "AIzaSyDdQAr26mxyCZFLPWk6NlxkL0TTHLMZ-v8",
    authDomain: "move-concerts.firebaseapp.com",
    databaseURL: "https://move-concerts.firebaseio.com",
    projectId: "move-concerts",
    storageBucket: "move-concerts.appspot.com",
    messagingSenderId: "333553856105"
  };

  firebase.initializeApp(config);

  var service = this;

  var auth = firebase.auth();
  var database = firebase.database();
  var storage = firebase.storage();

  service.database = database;

  /*  _         _   _
     / \  _   _| |_| |__
    / _ \| | | | __| '_ \
   / ___ \ |_| | |_| | | |
  /_/   \_\__,_|\__|_| |_|*/

  /**
   * Verificar si el usuario esta autenticado
   */
  service.getAuth = function() {
    return $q(function(resolve, reject) {
      auth.onAuthStateChanged(function(user) {
        if (user) resolve(user);
        else reject();
      });
    });
  };

  /**
   * Permite obtener la información del usuario
   */
  auth.onAuthStateChanged(function(user) {
    service.user = user || null;
  });

  /**
   * Permite crear un usuario por usuario y contraseña.
   * @returns Promise
   */
  service.createUser = function(email, password) {
    return auth.createUserWithEmailAndPassword(email, password);
  };

  /**
   * Permite crear un usuario con Facebook.
   * @returns Promise
   */
  service.createUserWithFacebook = function(accessToken) {
    return firebase
      .auth()
      .signInWithCredential(
        firebase.auth.FacebookAuthProvider.credential(accessToken)
      );
  };

  /**
   * Permite actualizar los datos de sesión de un usuario.
   * @param {Object} data
   * @returns Promise
   */
  service.updateUser = function(data) {
    return user.updateProfile(data);
  };

  /**
   * Permite actualizar el email de un usuario.
   * @param {String} email
   * @returns Promise
   */
  service.updateEmail = function(email) {
    return user.updateEmail(email);
  };

  /**
   * Permite actualizar la contraseña de un usuario.
   * @param {String} password
   * @returns Promise
   */
  service.updatePassword = function(password) {
    return user.updatePassword(password);
  };

  /**
   * Permite loguear a un usuario por email y contraseña.
   * @param {String} email
   * @param {String} password
   * @returns Promise
   */
  service.login = function(email, password) {
    return $q(function(resolve, reject) {
      auth
        .signInWithEmailAndPassword(email, password)
        .then(function(user) {
          resolve(user.uid);
        })
        .catch(function(error) {
          reject(error);
        });
    });
  };

  /**
   * Permite loguear a un usuario de manera remota.
   * @returns Promise
   */
  service.anonymous = function() {
    return auth.signInAnonymously();
  };

  /**
   * Permite desloguear a un usuario.
   * @returns Promise
   */
  service.logout = function() {
    return auth.signOut;
  };

  /*____        _        _
   |  _ \  __ _| |_ __ _| |__   __ _ ___  ___
   | | | |/ _` | __/ _` | '_ \ / _` / __|/ _ \
   | |_| | (_| | || (_| | |_) | (_| \__ \  __/
   |____/ \__,_|\__\__,_|_.__/ \__,_|___/\___|*/

  /**
   * Permite obtener la referencia en la base de datos.
   * @returns String
   */
  service.path = function(index) {
    if (typeof index == "string") return index;
    else if (!index || index.length < -1) return "/";

    var ref = "";
    for (var i in index) {
      ref += index[i] + "/";
    }
    return ref;
  };

  /**
   * Permite obtener datos actualizando en tiempo real.
   * @param {String - Array} index
   * @param {Function} cb
   */
  service.on = function(index, cb) {
    database.ref(service.path(index)).on("value", function(snapshot) {
      cb(snapshot.val());
    });
  };

  /**
   * Permite obtener datos de la base de datos.
   * @param {String - Array} index
   * @returns Promise
   */
  service.get = function(index) {
    return $q(function(resolve, reject) {
      database
        .ref(service.path(index))
        .once("value")
        .then(function(snapshot) {
          resolve(snapshot.val());
        });
    });
  };

  /**
   * Permite establecer información en la referencia ingresada.
   * @param {String - Array} index
   * @param {Object} data
   * @returns Promise
   */
  service.set = function(index, data) {
    return database.ref(service.path(index)).set(data);
  };

  /**
   * Permite actualizar información en la referencia ingresada. Ideal para formularios cuando se usa el metodo `on`.
   * @param {String - Array} index
   * @param {Object} data
   * @returns Promise
   */
  service.update = function(index, data) {
    return database.ref(service.path(index)).update(data);
  };

  /*
   * Permite crear un nodo hijo de la referencia ingresada, con los datos enviados.
   * @param {String - Array} index
   * @param {Object} data
   * @returns Promise
   */
  service.push = function(index, data) {
    return $q(function(resolve, reject) {
      var push = database.ref(service.path(index)).push(data);
      push.then(
        function() {
          resolve(push.key);
        },
        function(error) {
          reject(error);
        }
      );
    });
  };

  /**
   * Permite borrar información en la referencia seleccionada.
   * @param {Object} index
   * @returns Promise
   */
  service.remove = function(index) {
    return database.ref(service.path(index)).remove();
  };

  /*
   ____  _
  / ___|| |_ ___  _ __ __ _  __ _  ___
  \___ \| __/ _ \| '__/ _` |/ _` |/ _ \
   ___) | || (_) | | | (_| | (_| |  __/
  |____/ \__\___/|_|  \__,_|\__, |\___|
                            |___/      */

  /**
   * Permite cargar archivos.
   * @param {String - Array} index "usuario/info/foto"
   * @param {Array} files
   * @returns Promise
   */
  service.upload = function(index, file, cb) {
    return $q(function(resolve, reject) {
      var uploadTask = storage
        .ref(service.path(index))
        .child(file.$ngfName)
        .put(file);
      uploadTask.on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        function(snapshot) {
          if (cb !== undefined)
            cb((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        },
        function(error) {
          reject(error);
        },
        function() {
          resolve(uploadTask.snapshot.downloadURL);
        }
      );
    });
  };

  return service;
});
