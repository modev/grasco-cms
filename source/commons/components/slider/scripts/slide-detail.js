var cms = angular.module('cms');

function SlideDetailController() {
    var ctrl = this;

    ctrl.tpl = {
        video: 'source/commons/components/slider/templates/tpl-video.html',
        image: 'source/commons/components/slider/templates/tpl-image.html'
    };

    ctrl.getTpl = function () {
      return ctrl.tpl[ctrl.slide.type];
    };

    ctrl.delete = function () {
      ctrl.onDelete(ctrl.slide);
    };
}

cms.component('slideDetail', {
    templateUrl: 'source/commons/components/slider/templates/slide-detail.html',
    controller: SlideDetailController,
    bindings: {
        slide: '<', // one-way data binding
        onDelete: '&', // Outputs
        onUpdate: '&'
    }
});