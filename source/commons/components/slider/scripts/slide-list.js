var cms = angular.module('cms');

function SlideListController($timeout) {
    var ctrl = this;

    ctrl.config = {
        tpl: {
            bar: 'source/commons/components/slider/templates/slider-bar.html'
        }
    };

    $timeout(function () {
        ctrl.slides = ctrl.slides || [];
    }, 10);

    /**
     * Agregar un slide que puede ser de tipo video o imagen
     * @param type {video, image}
     */
    ctrl.add_slide = function (type) {
        if (!type) return;
        var slide = { url: '', type: type };
        ctrl.slides.push(slide);
    };

    ctrl.removeSlide = function (slide) {
        var idx = ctrl.slides.indexOf(slide);
        if (idx >= 0) {
            ctrl.slides.splice(idx, 1);
        }
    };
}

cms.component('slideList', {
    templateUrl: 'source/commons/components/slider/templates/slide-list.html',
    controller: SlideListController,
    bindings: {
        slides: '='
    }
});