var users = angular.module('users', ['ngTableToCsv', 'ngCsvImport']);

users.controller("UsersCtrl", function($scope, $firebase, $state, $filter, $commons, $q, $timeout, $http) {

    $scope.title = "ng-table-to-csv";
    $scope.data = {
        users: [],
        profes: [],
        admins: [],
        expositores: [],
        json: {},
        loading: true
    };

    $scope.types = {};
    $scope.csv = {};

    /**
     * traer datos
     */
    $firebase.database.ref('settings').child('user_types').on('value', function(data) {
        $scope.types = data.val();
    });

    $firebase.database.ref('users').once('value', function(data) {
        $scope.data = {
            users: [],
            profes: [],
            admins: [],
            json: {},
            loading: true
        };

        var info = data.val();
        $scope.usuarios = $filter('toArray')(info, true);

        angular.forEach($scope.usuarios, function(val) {
            if (!val.rol || val.rol == 'paid') {
                $scope.data.users.push(val);
            } else if (val.rol == 'profe') {
                $scope.data.profes.push(val);
            } else if (val.rol == 'admin') {
                $scope.data.admins.push(val);
            } else {
                $scope.data.expositores.push(val);
            }
        });

        $timeout(function() {
            $commons.apply($scope);
            $scope.data.loading = false;
        }, 500)
    });

    /**
     * check manual
     */
    $scope.form = {};
    $scope.methods = {
        check: function(id, type, company, email, name, documento) {
            return $q(function(resolve, reject) {
                $firebase.database.ref('users').child(id).update({
                    rol: 'paid',
                    info: {
                        email: email,
                        name: name,
                        cc: documento,
                        type: type || null,
                        company: company || null
                    }
                }).then(function() {
                    resolve();
                    $scope.methods.sendEmail(id, name, email, documento);
                });
            })
        },
        register: function(email, documento, name, type, company) {
            return $q(function(resolve, reject) {
                $firebase.createUser(email, documento).then(function(userData) {
                    var uid = userData.uid;
                    $scope.methods.updateUserName(name, uid);
                    $firebase.database.ref('users').child(uid).set({
                        info: {
                            name: name,
                            cc: documento,
                            email: email,
                            company: company,
                            type: type
                        },
                        rol: 'paid'
                    }).then(function() {
                        resolve();
                        $scope.methods.sendEmail(uid, name, email, documento);
                    });
                });
            })
        },
        addStaff: function(form, rol) {
            $scope.data.loading = true;
            $firebase.createUser(form.email, form.documento).then(function(userData) {
                var uid = userData.uid;
                $firebase.database.ref('users').child(uid).set({
                    info: {
                        name: form.name,
                        cc: form.documento,
                        email: form.email
                    },
                    rol: rol
                }).then(function() {
                    $scope.form = {};
                    $scope.data.loading = false;
                });
            });
        },
        /**
         * Enviar email al usuario despues de hacer el registro
         * @param id
         * @param name
         * @param email
         * @param document
         */
        sendEmail: function(id, name, email, document) {
            $http.post('certificados/email_datos_de_usuario.php', { id: id, name: name, email: email, document: document }).then(function(data) {
                console.log('mail sent:', data);
                console.log('user id:', id);
            })
        },
        /**
         * actualizar nombre de usuario
         */
        updateUserName: function(name, id) {
            $http.post('http://localhost:3100/' + id + '/' + name).then(function() {
                console.log('success:', id);
            }).catch(function(err) {
                console.log('err:', err);
                console.log('id:', id);
            });
        }
    };

    /**
     * tabs
     */
    //tabs y paneles de menu
    $scope.tab = 'all'; //tab activo por defecto: general

    $scope.SelectMenuItem = function(setTab) {
        $scope.tab = setTab;
    };

    $scope.isSelected = function(check) {
        return $scope.tab === check;
    };

    /**
     * Validar usuarios con subida de archivo
     */
    var content = false;
    $scope.upload = function() {
        if (!content && $scope.csv.content) {
            $scope.data.loading = true;
            content = true;
            var jsondata = angular.fromJson($commons.CSV2JSON($scope.csv.content));

            var CheckPromesas = [];

            angular.forEach(jsondata, function(val) {
                if (!val.documento) return console.log('val:', val);
                var doc = val.documento.replace(/,/g, '').replace(/\./g, '').replace(/\s/g, '');
                /**
                 * Query: buscar usuarios por numero de documento
                 */
                var check = [],
                    promesa = null;
                angular.forEach($scope.data.users, function(user) {
                    if (doc == user.info.cc) {
                        // el usuario existe hacer check
                        check.push(user);
                    }
                });

                var lastName = val.apellidos || '';
                var name = val.nombres + ' ' + lastName;

                if (check.length > 0) {
                    var _tempuser = check[0];
                    // existe
                    // $scope.methods.updateUserName(name, check[0].$key);
                    if (_tempuser.info.type) return;
                    console.log('email:', val.email);
                    promesa = $scope.methods.check(check[0].$key, val.tipo, val.empresa || '', _tempuser.info.email, _tempuser.info.name, _tempuser.info.cc);
                } else {
                    // register
                    console.log('email:', val.email);
                    promesa = $scope.methods.register(val.email, doc, name, val.tipo, val.empresa || '');
                }

                CheckPromesas.push(promesa);
            });

            $q.all(CheckPromesas).then(function() {
                console.log('Todos los usuarios cargados y/o registrados');
                $scope.data.loading = false;
                $scope.csv = {};
                content = false;
            });
        }
    };


});