var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('dashboard.users', {
    url: 'users',
    templateUrl: 'source/users/views/index.tpl.html',
    controller: 'UsersCtrl',
    resolve: {
      dashboard: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load(['source/users/controllers/users.js'], {cache: false});
      }]
    }
  });

});