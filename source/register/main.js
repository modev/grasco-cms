var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('register', {
    url: "/register",
    templateUrl: 'source/register/register.tpl.html',
    controller: 'RegisterCtrl',
    resolve: {
      register: ['$ocLazyLoad', function($ocLazyLoad){
        return $ocLazyLoad.load(['source/register/register.js'], {cache: false});
      }]
    }
  });

});
