var register = angular.module('register', []);

register.controller('RegisterCtrl', function($scope, $state, $firebase, $localStorage, $http, $commons){
  $scope.form = {};

  /**
   * Creación de nueva cuenta para evius.
  */
  $scope.submit = function(){
    $firebase.createUser({
      email: $scope.form.email,
      password: $scope.form.password
    }, function(error, auth){
      if(error && error.code === 'EMAIL_TAKEN'){
        $scope.showError('Este email ya se encuentra registrado');
        return;
      }

      var organization = $firebase.child("organizations").push({ name: $scope.form.organization, identification: $scope.form.identification, address: $scope.form.address});
      organization.child('administrators').push(auth.uid);

      $firebase.child("users").child(auth.uid).set({
        info: { email: $scope.form.email, name: $scope.form.name },
        account: { organization: organization.key(), role: 'administrator' }
      });

      $commons.sendMail('register', $scope.form.email).then(function(response){
        console.log(response);
        $scope.showSuccess('Se ha enviado un correo electrónico de verificación, no olvide revisar la carpeta Spam.');
      }, function(error){
        console.error(error);
        $scope.showError('Hemos tenido problemas con tu registro, por favor envíanos un correo a evius@mocionsoft.com sobre del código F37');
      });

    });
  }

  /**
   * Mostrar alerta de error.
  */
  $scope.showError = function(message){
    $scope.success = 'none';
    $scope.error = 'block';
    $scope.message = message;
    $commons.update($scope);
  }

  /**
   * Mostrar alerta de completado.
  */
  $scope.showSuccess = function(message){
    $scope.success = 'block';
    $scope.error = 'none';
    $scope.message = message;
    $commons.update($scope);
  }

});
