var publication = angular.module('cms');

publication.controller("PublicationCtrl", function($scope, $state, $commons, $firebase, $stateParams){

  /**
   * Este condicional permite evaluar si existen las variables para ejecución, de lo contrario devuelve a los eventos de la organización.
  */
  var id_evento = $stateParams.event_id;

  if(!id_evento)
    return $state.go('dashboard.events');

  /**
   * Permite obtener los datos del evento.
  */
  var ref = $firebase.database.ref('events').child(id_evento).child('settings');

  ref.on("value", function(snapevent){
    $scope.settings = snapevent.val();
    $commons.apply($scope);
  });

  $scope.update_status = function () {
      var status = ($scope.settings && $scope.settings.status) ? true : null;
      ref.update($scope.settings);
      $firebase.database.ref('active_events').child(id_evento).set(status);
  }

});
