var event = angular.module('cms');

event.controller('ModulesCtrl', function($log, $scope, $state, $stateParams, $firebase, $localStorage, $commons, $currentEvent){
  /**
   * Este condicional permite evaluar si existen las variables para ejecución, de lo contrario devuelve a los eventos de la organización.
  */
  var id_evento = $stateParams.event_id;

  if(!id_evento)
    return $state.go('dashboard.events');

  /**
   * Almacenar datos del evento actual
  */
  $scope.currentEvent = $currentEvent;

  var EventRef = $firebase.database.ref('events').child(id_evento);

  /**
   * Permite obtener los datos del evento.
  */
  EventRef.on("value", function(snapevent){
    $scope.event = snapevent.val();
    $scope.event.modules.tabs =  $scope.event.modules.tabs || [];
    $scope.event.modules.menu = $scope.event.modules.menu || [];
    /**
        * Guardamos los datos del evento en un servicio
        * para consultarlos mas adelante en algún modulo (programa)
        */
    $scope.currentEvent.data = snapevent.val();
    $commons.apply($scope);
  });

  /**
   * Permite obtener los datos de los modulos disponibles.
  */
  $scope.save = function($index, option){
    switch(option) {
      case 'availables':
        $scope.event.modules.availables.splice($index, 1);
        break;
      case 'menu':
        $scope.event.modules.menu.splice($index, 1);
        break;
      case 'tabs':
        $scope.event.modules.tabs.splice($index, 1);
        break;
    }

    EventRef.set($commons.objectClean($scope.event));
  };

  /**
  * Logica para creación de paginas
  */
  $scope.OpenPageForm = false;
  $scope.pageName = null;
  $scope.newPage = function (name) {

    $firebase.database.ref('pages').push({name: name}).then(function (snap) {

      var key = snap.key;

      EventRef.child('modules').child('availables').once('value', function(snapModules){
        var availables = snapModules.val();
        availables[availables.length] = {editable: true, icon: 'ion-ios-paper-outline', login: false, name: name, state: 'pages', id: key};
        EventRef.child('modules').child('availables').set(availables);
      });

      $scope.pageName = null;
      $scope.OpenPageForm = false;

    });
  };
});
