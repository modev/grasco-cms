var event = angular.module('cms');

event.service('$footer', function(){
  return {
    data: {
      current: null,
      event: null
    },
    set: function(current, event){
      data.current = current;
      data.event = event;
    }
  }
});

event.controller('EventCtrl', function($scope, $footer, $commons, $currentEvent){
  $scope.currentEvent = $currentEvent;

  /**
  * validamos la configuracion del evento
  * requerido: nombre y fecha
  */
  $scope.$watch('currentEvent.data.info', function(newVal, oldVal) {
    if(newVal == undefined) return;
    (newVal.name && newVal.dateFrom) ? $scope.currentEvent.valid.config = true : $scope.currentEvent.valid.config = false;
  }, true);

  /**
  * validamos los modulos del evento
  * que tenga al menos un modulos asignado a menu o tabs
  */
  $scope.$watch('currentEvent.data.modules', function(newVal, oldVal) {
    if(newVal == undefined) return;
    (newVal.menu || newVal.tabs) ? $scope.currentEvent.valid.modules = true : $scope.currentEvent.valid.modules = false;
  }, true);

  //old-logic
  $scope.footer = $footer.data;
});
