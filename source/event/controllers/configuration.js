var event = angular.module('cms');

event.filter('orderByOrder', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});

event.controller('ConfigurationCtrl', function($scope, $state, $stateParams, $firebase, $commons, $currentEvent, _networks){

    var ctrl = this;

    ctrl.contenido = [];

    $scope.sending = false;
    /**
    * Servicio para compartir los datos del evento actual
    */
    $scope.currentEvent = $currentEvent;

    /**
    * Este condicional permite evaluar si existen las variables para ejecución, de lo contrario devuelve a los eventos de la organización.
    */
    var id_evento = $stateParams.event_id;

    if(!id_evento)
       return $state.go('dashboard.events');

    $scope.currentEvent.id = id_evento;
    var EventRef = $firebase.database.ref('events').child(id_evento);


    /**
    * Este condicional permite obtener la información de un evento en vivo, en caso de que sea un nuevo evento se crea y de igual manera se otiene la información en vivo.
    */
    EventRef.on("value", function(snapevent){
        $scope.models = {
            lists: []
        };
        ctrl.long = snapevent.child('content').numChildren();
        $scope.event = snapevent.val();
        angular.forEach($scope.event.contenido, function (value) {
            $scope.models.lists.push(value);
        });
        /* Model to JSON for demo purpose
        $scope.$watch('models', function(model) {
            console.info(model);
            for(var i = 0; i < model.lists.length; i++){
                ctrl.model = {
                    type : model.lists[i].type,
                    content : model.lists[i].content
                };
                ctrl.contenido.push(ctrl.model);
            }

        }, true);*/
        if (!$scope.event) return;
        /**
        * Guardamos los datos del evento en un servicio
        * para consultarlos mas adelante en algún modulo (programa)
        */
        $scope.currentEvent.data = snapevent.val();
        $scope.currentEvent.checkDates();

        //Aplicar los cambios en el template
        $commons.apply($scope);
    });



    /**
    * Esta función permite guardar la información del formulario en tiempo real.
    */
    $scope.save = function(){
        $scope.sending = true;
        EventRef.update($commons.objectClean($scope.event), function(error) {
            // console.log('set info done');
            if(error) {
                console.log('error:', error);
            } else {

            }
            $scope.sending = false;
            $commons.apply($scope);
        });
    };

    /**
     * Esta función permite cargar la imagen del evento.
     */
    $scope.updateImage = function(file) {
        if(!file)
            return;

        $firebase.upload('event/images', file).then(function(response){
            $scope.event.info.image = response.data;
            $scope.save();
        }).catch(function (err) {
            console.warn(data);
        })
    };


    /**
     * Redes
     */
    $scope.networks = _networks;
    $scope.network = null;//guardamos red social activada
    //funcion determina cual red social esta activa
    $scope.selectNetwork = function(setNetwork) {
        $scope.network = setNetwork;
    };
    //revisamos si la red social esta activa y retornamos in valos boleano
    $scope.isSelected = function(checkNetwork) {
      return $scope.network === checkNetwork;
    };

    /**
     * Contenido (texto, url video)
     */
    var ContentRef = EventRef.child('contenido');
    var ContentOrder = ContentRef.orderByChild('_order');
    var dinamic_content = {
        templates: {
            video: 'source/event/views/templates/video.html',
            content: 'source/event/views/templates/content.html',
            image: 'source/event/views/templates/image.html'
        }
    };
    $scope.get_content_tpl = function (type) {
      return dinamic_content.templates[type];
    };
    /**
     * Agregar contenido dinamico
     * @param type (video, content)
     */
    $scope.add_content = function (type) {
        if (!type) return;
        var newcontent = ContentRef.push().key;
        ContentRef.child(newcontent).update({ id : newcontent, type: type, _order : ctrl.long+1 });
    };

    /**
     * Cambiar orden
     */
    $scope.up_item = function (llave, orden) {
        if(orden == 1)
            return;
        var order_up = orden - 1;
        ContentOrder.startAt(order_up)
            .endAt(order_up)
            .once('value', function (snap) {
                var data = snap.val();
                angular.forEach(data, function (value) {
                    console.log(value);
                    console.info('Llave '+llave,' '+value.id);
                     ContentRef.child(value.id).update({_order : orden});
                     ContentRef.child(llave).update({_order : order_up});
                });
            });
    };
    $scope.down_item = function (llave, orden) {
        if(orden == ctrl.long)
            return;
        var order_down = orden + 1;
        ContentOrder.startAt(order_down)
            .endAt(order_down)
            .once('value', function (snap) {
                var data = snap.val();
                angular.forEach(data, function (value) {
                    console.log(value);
                    console.info('Llave '+llave,' '+value.id);
                    ContentRef.child(value.id).update({_order : orden});
                    ContentRef.child(llave).update({_order : order_down});
                });
            });
    };

    /**
     * borrar items
     */
    $scope.remove_item = function (index) {
        console.log(index);
        ContentRef.child(index).remove();
    };
});
