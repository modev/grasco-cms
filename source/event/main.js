var cms = angular.module('cms');

cms.config(function ($stateProvider, $ocLazyLoadProvider) {

    $stateProvider.state('dashboard.event', {
        abstract: true,
        url: "event/",
        templateUrl: 'source/event/views/index.tpl.html',
        controller: 'EventCtrl',
        resolve: {
            event_id: ['$stateParams', function ($stateParams) {
                return $stateParams.event_id;
            }],
            dashboard: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load(['source/event/controllers/index.js'], {cache: false});
            }]
        },
        params: {
            event_id: null
        }
    });

    $stateProvider.state('dashboard.event.configuration', {
        url: "configuration",
        templateUrl: 'source/event/views/configuration.tpl.html',
        controller: 'ConfigurationCtrl',
        resolve: {
            dashboard: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load(['source/event/controllers/configuration.js'], {cache: false});
            }]
        }
    });

    $stateProvider.state('dashboard.event.modules', {
        url: "modules",
        templateUrl: 'source/event/views/modules.tpl.html',
        controller: 'ModulesCtrl',
        resolve: {
            dashboard: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load(['source/event/controllers/modules.js'], {cache: false});
            }]
        }
    });

    $stateProvider.state('dashboard.event.publication', {
        url: "publication",
        templateUrl: 'source/event/views/publication.tpl.html',
        controller: 'PublicationCtrl',
        resolve: {
            dashboard: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load(['source/event/controllers/publication.js'], {cache: false});
            }]
        }
    });

});
