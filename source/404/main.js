var cms = angular.module('cms');

cms.config(function($stateProvider, $ocLazyLoadProvider){

  $stateProvider.state('notfound', {
    url: "/404",
    templateUrl: 'source/404/notfound.tpl.html',
    controller: function($scope, $state, $firebase){
      $scope.route = ($firebase.getAuth() === null) ? 'login' : 'dashboard.events';
    }
  });

});
