'use strict';

module.exports = function(grunt) {
    // cargamos todas las tareas
    require('load-grunt-tasks')(grunt);

    // Configuracion del proyecto.
    grunt.initConfig({
        wiredep: {
            app: {
                src: ['index.html']
            }
        },
        includeSource: {
            dev: {
                files: {
                    //dest - src
                    'index.html': 'index-templates/index.dev.html'
                }
            },
            dist: {
                files: {
                    //dest - src
                    'index.html': 'index-templates/index.dist.html'
                }
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            libs: {
                files: { '.tmp/libs/libs.js': require('wiredep')().js }
            },
            appMain: {
                files: { '.tmp/js/app-main.js': ['source/main.js', 'source/*/*.js'] }
            },
            appModules: {
                files: { '.tmp/js/app-modules.js': 'source/modules/*/*.js' }
            }
        },
        concat: {
            libs: {
                src: ['.tmp/libs/libs.js'],
                dest: '.tmp/libs/libs.pack.js'
            },
            app: {
                src: ['.tmp/js/app-main.js', '.tmp/js/app-modules.js'],
                dest: '.tmp/js/main.pack.js'
            }
        },
        uglify: {
            libs: {
                src: ['.tmp/libs/libs.pack.js'],
                dest: 'build/libs/libs.min.js'
            },
            app: {
                src: ['.tmp/js/main.pack.js'],
                dest: 'build/js/app.min.js'
            }
        },
        clean: {
            tmp: '.tmp',
            build: 'build'
        }
    });

    // resgistrar las tareas
    grunt.registerTask('default', [
        'dev'
    ]);

    grunt.registerTask('dev', [
        'clean',
        'includeSource:dev',
        'wiredep'
    ]);

    grunt.registerTask('build', [
        'clean',
        'ngAnnotate',
        'concat',
        'uglify',
        'includeSource:dist',
        'clean:tmp'
        //'wiredep'
    ]);
};
